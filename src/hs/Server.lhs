<?xml version="1.0" encoding="UTF-8"?>
<module>

<paragraph>
<title>A Simple Test Server</title>
This tiny module had been create for testing sending an receiving messages. 
<haskell><![CDATA[
\begin{code}
module Main where 
import Z21.Protocoll

import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString

import Numeric (showHex)
import qualified Data.ByteString as C

import Control.Monad (forever, when)
import Data.IORef

port = 21105
host = "0.0.0.0"

type Host = SockAddr

main = withSocketsDo $ do
        s <- socket AF_INET Datagram defaultProtocol
        bindAddr <- inet_addr host
        bindSocket s (SockAddrInet port bindAddr)

        hostsRef <- newIORef []
        forever $ do
                (msg, hostAddr) <- recvFrom s 1024
                putStrLn $ (show hostAddr)
                putStrLn$show$map (\x ->  showHex x "")$ C.unpack msg
                print$readMessage msg
                hosts <- readIORef hostsRef
                when (notElem hostAddr hosts) $ modifyIORef hostsRef (hostAddr:)
                hosts <- readIORef hostsRef
                sendToAll s ( msg) $ hosts
        sClose s

-- sendToAll :: Socket -> String -> [Host] -> IO ()
sendToAll socket msg hosts = do
        mapM_ (sendTo socket msg) $ hosts
\end{code}
]]></haskell>
</paragraph>
</module>

