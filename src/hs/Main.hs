{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Applicative
import Snap.Core
import Snap.Util.FileServe
import Snap.Http.Server

import Network.WebSockets      
import Network.WebSockets.Snap 

import Data.ByteString.UTF8 (fromString) 

import Control.Monad.IO.Class

import Control.Concurrent.Timer
import Control.Concurrent
import Control.Concurrent.Suspend.Lifted

-- Network library
import Network.Socket hiding (send, sendTo, recv, recvFrom)

--prog args and such
import System.Environment


import Z21.Protocoll hiding(light,direction, speed)
import Z21.State
import Z21.MessageEventListener
import Z21.Constants

keepAlive sendF = do
  sendF  LAN_GET_SERIAL_NUMBER
  repeatedTimer (sendF LAN_GET_SERIAL_NUMBER >>return ())$sDelay 10


main :: IO ()
main = do
  -- process arguments
  args <- getArgs
  let (host:portA:_) =
        if length args < 2
        then [_DEFAULT_CLIENT,show _DEFAULT_PORT]
        else args

  -- network stuff
  let port = fromInteger (read portA)
  sock <- socket AF_INET Datagram defaultProtocol
  let bindAddr = tupleToHostAddress (0,0,0,0)-- inet_addr "0.0.0.0"
  let hostAddr = tupleToHostAddress (192,168,178,111) --inet_addr host

  bind sock (SockAddrInet port bindAddr)  
  let addr = (SockAddrInet port hostAddr)  
  let sendF = sendMsg sock addr
      
  -- keep alive timer
  keepAliveThread <- keepAlive sendF

  -- synchronized state variable
  state <- newMVar newState

  eventListener <- newEventListener
--  addListener eventListener $processMessage gui state sendF
  eventThread <- startEventListener eventListener sock
  
  quickHttpServe (site sock sendF)

--site :: ( Message -> IO Int) -> Snap ()
site sock sendF =
    route [ ("message/:messageparam", messageHandler sendF)
          , ("/",(serveDirectory "."))
          , ("/ws", runWebSocketsSnap (socketApp sock))
          ] 


socketApp sock pending = do
  con <- acceptRequest pending
  listener <- newEventListener
  addListener listener (wsListener con)
  oneShotTimer (sendTextData con (fromString "WEBSOCKET")) (sDelay 2)  
  receiveMessages listener sock 
  
wsListener con msg = do
  sendTextData con (fromString$show msg)
  return True

--messageHandler :: ( Message -> IO Int) -> Snap ()
messageHandler sendF = do
    param <- getParam "messageparam"
--    liftIO$print  param
    let paramString = (fmap (dropLast.tail.show) param)::(Maybe String)
    let msg = (fmap read paramString)::Maybe  Z21.Protocoll.Message
    liftIO$sendToZ21 sendF$maybe LAN_GET_SERIAL_NUMBER id msg 
    maybe (writeBS "<error>must specify echo/param in URL</error>")
          (\x-> writeBS (fromString ( "<okay>"++show x++"</okay>"))) param

sendToZ21 sendF (LAN_X_SET_TURNOUT nr _ abzweig) = do
  let v = abzweig
  print "und nun die härte der weiche"
  sendF$LAN_X_SET_TURNOUT nr  True  v
  print "und nocheinmal"
  oneShotTimer ((sendF$LAN_X_SET_TURNOUT nr  False v)
                >>sendF (LAN_X_GET_TURNOUT_INFO nr)
                >>print "scheint zu klappen....") (sDelay 1)  
  return 1
sendToZ21 sendF msg = do
  print msg
  sendF msg


dropLast [x] = []
dropLast (x:xs) = x:dropLast xs
dropLast [] = []
