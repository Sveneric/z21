<?xml version="1.0" encoding="UTF-8"?>
<module>
<paragraph>
<title>Automatic Commuting</title>
A typical usecase for a modell railway display is  a train commuting between two stops. In German there is the nice word: <em>Pendelzugautomatik</em>. To put this into realization we need some contacts that signifies when a train reaches a certain point. We can use circuit tracks for this purpose. 

This module implements some means to program a Pendelzugautomatik. It provides a function, which will start a Pendelzugautomatik and a GUI component, to start a commuting train.
<code><![CDATA[
\begin{code}
module Z21.Commuting where

import Z21.State
import Z21.Protocoll
import Z21.MessageEventListener

import Data.Maybe

import Text.Read

import Control.Concurrent.MVar
import Control.Concurrent.Timer
import Control.Concurrent.Suspend.Lifted

import Data.Bits

import Graphics.UI.Gtk
\end{code}
]]></code>

The Z21 signifies contact on a circuit track with a <tt>LAN_RMBUS_DATACHANGED</tt> message. It uses the so called R-Bus for feedback modules. The 
circuit tracks are grouped, such that a contact is represented by a pair of numbers: the group and the address. 

<code><![CDATA[
\begin{code}
type Contact = (Int,Int)
\end{code}
]]></code>

To start a Pendelzugautomatik we need basic information between which stops, which loco and the idle time at each stop. Furthermore we need global information: the event listeners and the state of the control unit. And of course the communication function with the z21 control unit. 

The result is an IO action which returns the number of the event listener, that controls the Pendelzugautomatik.
Thus we get the following type<footnote>The actual derived type is more general, but for a clearer understanding it has been simplified a bit.</footnote>.
<code><![CDATA[
\begin{code}
startCommuting
  ::   Contact              -- first contact
    -> Contact              -- second contact
    -> Int                  -- loco id
    -> Int                  -- idle time
    -> MVar MessageListener -- event listeners
    -> MVar State           -- global state
    -> (Message -> IO a1)   -- communication with Z21
    -> IO Integer           -- return the number of the event listener
\end{code}
]]></code>

The function <tt>startCommuting</tt> is basically implemented by a new event listener function. 
This function will evaluate <tt>LAN_RMBUS_DATACHANGED</tt> messages. In order to recieve these messages from the Z21 control unit, we need to send an appropiate 
<tt>LAN_SET_BROADCASTFLAGS</tt> message. 

We assume that the locomotive is somewhere between the two stops. The locomotive will be started in one arbitrary direction.

A further state variable is used to remember which was the last stop the loco activated. It is initialized with the illegal contact <tt>(-1,-1)</tt>.

<code><![CDATA[
\begin{code}
startCommuting leftC rightC locid delay eventListener state send = do
  lastContact <- newMVar (-1,-1)
  send LAN_SET_BROADCASTFLAGS
             {general=True,rbus=True,systemState=False}

  nr <- addListener eventListener (eval lastContact)

  send (LAN_X_SET_LOCO_DRIVE locid 2 6 Forward)

  modifyMVar_ state (\s -> return$setDirectionOfLoco locid Forward s)
  return nr
\end{code}
]]></code>

The main work is done by the event listener function. It has two arguments. The state variable, which contains the circuit track that has been contacted the last time. The second argument is the message to be processed.

First of all the function needs to analyse the incoming message. Which contact is active. Wen need to have a closer look at the single bytes of the 
message. <footnote>It would have been more consequent to have done this in the module <tt>Protocoll</tt>  and provided more structured information with the constructor <tt>LAN\_RMBUS\_DATACHANGED</tt>.</footnote>


The first local definitions create a list of all active contacts. As a matter of fact the message may contain information on several activ contacts. Currently the implementation neglects simultanious contacts in different groups. This is simply duw to the fact that I have not enough hardware to test several groups.

<code><![CDATA[
\begin{code}
   where
     eval lastContact (LAN_RMBUS_DATACHANGED (gi:gs) ) = do
       let adder = if gi==0 then 0 else 10
       let numbered=filter (\(_,entry)-> entry/=0)$zip [1..] gs
       if (null numbered) then return False else do
       let changedGroup = adder+fst(head numbered)
       let groupVal = snd(head numbered)
       let addrs = filter (\(_,code)-> code == groupVal.&.code)
                    $zip
                     [(1::Int)..]
                     [0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80]
       if (null addrs)  then return False else do
       let newContacts = map (\(x,_)->(changedGroup,x)) addrs
\end{code}
]]></code>

First of all we check if the last contact we reacted to is still  active or again  active.
If this is the case the locomotive obviously has not moved very much and still triggers the 
contact of the same stopping. Then we do not react at all.

<code><![CDATA[
\begin{code}
       oldContact <- readMVar lastContact
       if (elem oldContact newContacts) then return False else do
\end{code}
]]></code>

If none of the contacts at the two stoppings is active then no reaction is necessary.

<code><![CDATA[
\begin{code}
       if not (elem leftC newContacts || elem rightC newContacts)
         then return False else do
\end{code}
]]></code>

Eventually at this point we know that the train reached the other stopping. No several things are to be done. Stop the train. Modify the global state. After the specified time of delay start the train with the switched direction. The wait is done with the timer function: <tt>oneShotTimer</tt>.

<code><![CDATA[
\begin{code}
       let newContact = if elem leftC newContacts 
                        then leftC 
                        else rightC

       modifyMVar_ lastContact (\_->return newContact)

       s <- readMVar state
       let dir = switchDirection$getDirectionOfLoco locid s
       modifyMVar_ state (\s -> return$setDirectionOfLoco locid dir s)

       send (LAN_X_SET_LOCO_DRIVE locid 2 0 dir)

       oneShotTimer 
         ((send$LAN_X_SET_LOCO_DRIVE locid 2 6 dir) >>return ()) 
         (sDelay $fromIntegral delay)
       return True
\end{code}
]]></code>
We are only interested in <tt>LAN_RMBUS_DATACHANGED</tt> messages. For all other message no reaction is necessary.
<code><![CDATA[
\begin{code}
     eval _ _ = return False
\end{code}
]]></code>

Since the Pendelzugautomatik is controlled by a single event listener function the Pendelzugautomatik can be stopped by removing this listener from the global listener queue.

<code><![CDATA[
\begin{code}
stopCommunting listenId locid eventListener send = do      
  send (LAN_X_SET_LOCO_DRIVE locid 2 0 Forward)
  removeListener eventListener listenId 
\end{code}
]]></code>

<paragraph>
<title>GUI</title>
We provide a GUI component for the control of commuting trains. We apply the same pattern as in the module <tt>Z21.Gui</tt>. All relevant controls are collected in one type.

<haskell><![CDATA[
\begin{code}
data -- BoxClass a =>
   CommutingGui a = CommutingGui
   { mainPanel        :: VBox
   , addrEntry        :: Entry
   , fstCircuitGroup  :: Entry
   , fstCircuitEntry  :: Entry
   , sndCircuitEntry  :: Entry
   , sndCircuitGroup  :: Entry
   , delayEntry       :: Entry
   , startStopControl :: ToggleButton
   }
\end{code}
]]></haskell>

A constructor function is provided.
<haskell><![CDATA[
\begin{code}
newCommutingGui = do
  mainPanel       <- vBoxNew False 10
  addEntry        <- entryNew
  fstCircuitEntry <- entryNew
  entrySetText fstCircuitEntry "1"
  sndCircuitEntry <- entryNew
  entrySetText sndCircuitEntry "2"  
  fstCircuitGroup <- entryNew
  entrySetText fstCircuitGroup "1"
  sndCircuitGroup <- entryNew
  entrySetText sndCircuitGroup "1"
  delayEntry      <- entryNew
  entrySetText delayEntry "5"
  startStopButton <- toggleButtonNewWithLabel "Start/Stop"
  return
    CommutingGui    
      { mainPanel        = mainPanel
      , addrEntry        = addEntry
      , fstCircuitEntry  = fstCircuitEntry
      , sndCircuitEntry  = sndCircuitEntry
      , fstCircuitGroup  = fstCircuitGroup
      , sndCircuitGroup  = sndCircuitGroup
      , delayEntry       = delayEntry
      , startStopControl = startStopButton
      }      
\end{code}
]]></haskell>
<paragraph>
<title>Layout</title>
A layout is added to the components.

<haskell><![CDATA[
\begin{code}
mkLayoutCommutingGui gui = do
  lokP       <- hBoxNew False 10
  lokLabel   <- labelNew$Just "Lokadresse"
  boxPackStart lokP lokLabel PackNatural 0  
  boxPackStart lokP (addrEntry gui) PackNatural 0
  boxPackStart (mainPanel gui) lokP PackNatural 0  
  fstLabel   <- labelNew$Just "erster Kontakt (Gruppe,Nr)"
  boxPackStart (mainPanel gui) fstLabel PackNatural 0
  fstP       <- hBoxNew False 10
  boxPackStart fstP (fstCircuitGroup gui) PackNatural 0
  boxPackStart fstP (fstCircuitEntry gui) PackNatural 0
  boxPackStart (mainPanel gui) fstP PackNatural 0
  sndLabel   <- labelNew$Just "zweiter Kontakt (Gruppe,Nr)"
  boxPackStart (mainPanel gui) sndLabel PackNatural 0
  sndP       <- hBoxNew False 10
  boxPackStart sndP (sndCircuitGroup gui) PackNatural 0
  boxPackStart sndP (sndCircuitEntry gui) PackNatural 0
  boxPackStart (mainPanel gui) sndP PackNatural 0
  delayP     <- hBoxNew False 10
  delayLabel <- labelNew$Just "Wartezeit"
  boxPackStart delayP delayLabel PackNatural 0
  boxPackStart delayP (delayEntry gui) PackNatural 0
  boxPackStart (mainPanel gui) delayP PackNatural 0
  boxPackStart (mainPanel gui) (startStopControl gui) PackNatural 0
\end{code}
]]></haskell>
</paragraph>
<paragraph>

<title>Events</title>
And eventually we add event listeners to the controls. Internally we keep a state variable for the number of the listener that is active. It is initialized with the illegal number <tt>-1</tt>.

<haskell><![CDATA[
\begin{code}
addCommutingGuiEvents gui eventListener state send =  do
  let startStopB = startStopControl gui
  listenerNr  <- newMVar (-1::Integer)
  addrS <- entryGetText (addrEntry gui)

\end{code}
]]></haskell>
<paragraph>
<title>User Input</title>

Whenever the start/stop button is clicked, we first have a look if the button is active. 
<haskell><![CDATA[  
\begin{code}
  startStopB `onClicked` do
    mode  <- toggleButtonGetActive startStopB
\end{code}
]]></haskell>

If this is not the case then we stop the running commuting train.

<haskell><![CDATA[
\begin{code}
    if not mode then do
        nr <- readMVar listenerNr
        stopCommunting nr (read addrS) eventListener send
     else do
\end{code}
]]></haskell>
</paragraph>
<paragraph>
<title>Validation of User Input</title>
Otherwise we can read and validate the user input.
For validation of the user input we use the standard function <tt>readMaybe</tt>. Since the type <tt>Maybe</tt> is an instance of <tt>Monad</tt> we can use the do-notation for validation of the user input. If the user input cannot be evaluated we return with no action at all. 

<haskell><![CDATA[
\begin{code}
    fstCS <- entryGetText (fstCircuitEntry gui)
    sndCS <- entryGetText (sndCircuitEntry gui)
    fstGS <- entryGetText (fstCircuitGroup gui)
    sndGS <- entryGetText (sndCircuitGroup gui)
    delayS<- entryGetText (delayEntry gui)

    let inputValues =
          do
            a  <- (readMaybe addrS) ::Maybe Int
            fC <- (readMaybe fstCS) ::Maybe Int
            sC <- (readMaybe sndCS) ::Maybe Int
            fG <- (readMaybe fstGS) ::Maybe Int
            sG <- (readMaybe sndGS) ::Maybe Int
            d  <- (readMaybe delayS)::Maybe Int
            return (a,fG,fC,sG,sC,d)
    if (isNothing inputValues) then return () else do
\end{code}
]]></haskell>

Otherwise the user input can be used to start a Pendelzugautomatik.
<haskell><![CDATA[
\begin{code}
    let (Just (addr,fstG,fstC,sndG,sndC,delay)) = inputValues
    nr <- startCommuting (fstG, fstC) (sndG, sndC) addr delay 
              eventListener state send
    modifyMVar_ listenerNr (\_ -> return nr)
\end{code}
]]></haskell>
</paragraph>
</paragraph>
</paragraph>
</paragraph>
</module>
