<?xml version="1.0" encoding="UTF-8"?>
<module>
<paragraph>
<title>Global State</title>
This chapter contains the module <tt>State</tt>. It is the model of the application. The client will keep a global state. This state can be controlled through a graphical user interface. Furthermore the state can get modified through incoming messages from the z21 server. This will be the case, when other clients control trains.

<code><![CDATA[
\begin{code}
module Z21.State where
import Z21.Protocoll
  (Direction(..),SpeedSteps(..),switchDirection)
import Data.Word
\end{code}
]]></code>

<paragraph>
<title>Data Types</title>
The global state is basically a list of locomotive states. One locomotive is the currently controlled locomotive. This will not be included in the list of locomotives.

<code><![CDATA[
\begin{code}
data State = Z21 {currentLoco::Loco,locos::[Loco]}
\end{code}
]]></code>

A locomotive state is represented by its address, speed, direction, steps for speed and its light status.<footnote>We have a field for the address and a further field for some ID. Howerever, currently both are redundantely used. Maybe some day it might be nice to have a database of locomotives with own IDs and some description.</footnote>

<code><![CDATA[
\begin{code}
data Loco = Loco
            { lid::Int
            , address::Int
            , steps::SpeedSteps
            , speed::Word8
            , direction::Direction
            , light::Bool} deriving (Eq,Show)
\end{code}
]]></code>
</paragraph>
<paragraph>
<title>Construction</title>
Two functions are provided to create states.

<code><![CDATA[
\begin{code}
newState = let (l:ls) = map newLoco (201:[1..20])
           in Z21{currentLoco=l,locos=ls}

newLoco lid
  = Loco
     { lid=lid
     , address = lid
     , steps = S128
     , speed=0
     , direction=Forward
     , light=True}

newLocoInState st lid = st{locos=newLoco lid:locos st}
\end{code}
]]></code>
</paragraph>
<paragraph>
<title>Getter Functions</title>
Some convenient getter functions to retrieve values are provided.

<code><![CDATA[
\begin{code}
getDirectionOfLoco locoid st
  |locoid == (lid$currentLoco st) = direction$currentLoco st
  |otherwise = direction$head$filter (\loco->lid loco==locoid) $locos st

getAddress = address.currentLoco
getDirection  = direction.currentLoco
getSpeed  = speed.currentLoco
getLight = light.currentLoco

getLoco id [] = Nothing
getLoco id (l:ls)
  |id == lid l = Just l
  |otherwise = getLoco id ls
\end{code}
]]></code>

The following function is used to change the currently activ locomotive. It creates a new state. If the locomotive with the corresponding ID does not exist in the state, then a new locomotive is created.

<code><![CDATA[
\begin{code}
selectLoco locoid st
  |locoid == (lid$currentLoco st) = st
  |Nothing==loco = st{currentLoco=newLoco locoid
                     ,locos=(currentLoco st):locos st}
  |otherwise = let (Just (locs,loc)) = loco
               in st{currentLoco=loc,locos=(currentLoco st):locs}
  where
    loco = getLoco [] (locos st)
    
    getLoco locos [] = Nothing
    getLoco locos (l:ls)
      |lid l == locoid = Just (locos++ls,l)
      |otherwise = getLoco (l:locos) ls 
\end{code}
]]></code>
</paragraph>
<paragraph>
<title>Setter Functions</title>
In this section some setter functions are defined. Since in Haskell we cannot modify any data, these functions transform the state and return a new state-

<code><![CDATA[
\begin{code}
setDirection dir st = st{currentLoco=(currentLoco st){direction=dir}}
setSpeed sp st = st{currentLoco=(currentLoco st){speed=sp}}
setLight l st = st{currentLoco=(currentLoco st){light=l}}

replaceNonActiveLoco loco@Loco{lid=id} st@Z21{locos=ls}
  = st{locos=map (\l->if lid l == id then loco else l) ls}

changeDirectionOfLoco locoid st@Z21{currentLoco=current,locos=ls}
  |locoid == lid current
    = st{currentLoco=changeDirectionInLoco current}
  |otherwise = st{locos=map
                        (\loco-> if (lid loco==locoid) 
                                 then loco
                                 else changeDirectionInLoco loco) ls}

setDirectionOfLoco locoid dir st@Z21{currentLoco=current,locos=ls}
  |locoid == lid current
    = st{currentLoco=current{direction=dir}}
  |otherwise = st{locos=map
                        (\loco-> if (lid loco/=locoid) 
                                 then loco
                                 else loco{direction=dir}) ls} 
               

changeDirectionInLoco l = l{direction=switchDirection$direction l}
\end{code}
]]></code>
</paragraph>
</paragraph>

</module>
