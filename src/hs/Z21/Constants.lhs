<?xml version="1.0" encoding="UTF-8"?>
<module>
<paragraph>
<title>Constants</title>
The Z21 control has a fixed IP number and a fixed port. It even  demands the clients to use the same port.

<code><![CDATA[
\begin{code}
module Z21.Constants  where

_DEFAULT_CLIENT="192.168.0.111"
_DEFAULT_PORT=21105

\end{code}
]]></code>
</paragraph>
</module>

