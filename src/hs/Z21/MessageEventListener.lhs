<?xml version="1.0" encoding="UTF-8"?>
<module>
<paragraph>
<title>Message handling</title>
We are receiving messages from the Z21 control. Messages will notify contacts on curcuit tracks, new values for turn outs or locos.
In this module we define means to program reaction to recieved messages.

<haskell><![CDATA[
\begin{code}
module Z21.MessageEventListener where
import Z21.Protocoll hiding(light,direction, speed)
import qualified Z21.Protocoll as Z21(light,direction, speed)
import Z21.State
import Z21.Gui

import Control.Monad (forever)
import Control.Concurrent

import Graphics.UI.Gtk

--import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString
\end{code}
]]></haskell>

The main type is a list of message handlers. A message handlers is basically a function, which takes a messages and results an <tt>(IO Bool)</tt> event. This is an IO action that results in an boolean value. The boolean value signifies, if the message was of interest and had been successfully processed, e.g. there might be a message handler which waits for contact on a certain circuit track. Only a message signifying contact on this track will result in an succesfull IO operation.

Every message handler has a unique number and a boolean flag. The flag signifies, if the message handler will only be used one time successfully.

The message listener type has the list of message handlers and a number, which will be the number of the next message handler. Thus we can provide unique numbers for message handlers.
<haskell><![CDATA[
\begin{code}
type MessageHandler = (Integer,Bool,Message -> IO Bool)
type MessageListener = (Integer,[MessageHandler])
\end{code}
]]></haskell>
The overall message listener will be a global state variable initialized with the empty handler list.
<haskell><![CDATA[
\begin{code}
newEventListener = newMVar ((1,[])::MessageListener)
\end{code}
]]></haskell>

The main function for the global message listener will recieve the messages from some socket. It will process every message handler in he list. Afterwards every message handler which ended successful and has the flag will be deleted from the list.

<haskell><![CDATA[
\begin{code}
receiveMessages eventListener socket = forever $ do
    message <-recv socket 1024
    let msg = readMessage message
    print msg
    (nr,listener)<- readMVar eventListener
    rs <-sequence $ map (\(nr,_,f) -> f msg) listener 
    let toDelete = filter (\((nr,oneShot,_),shot)->oneShot && shot)
                   $zip (listener) rs 
    sequence_
     (map (\((nr,_,_),_)->removeListener eventListener nr) toDelete)
\end{code}
]]></haskell>

The function above will be started in an own thread.

<haskell><![CDATA[
\begin{code}
startEventListener listener socket = 
  forkIO$receiveMessages listener socket 
\end{code}
]]></haskell>

We provide two functions to add new message handlers to the global message listener. The result of the action is the number of the added handler.

Two versions are provided. One for handlers which will be removed after the first successful reaction, one for handlers that stay in the list.

<haskell><![CDATA[
\begin{code}
addListener = addListenerAux False
addOneTimeListener = addListenerAux True
   
addListenerAux oneTime eventListener f = do
  modifyMVar_ eventListener
    (\(nr,fs)->return (nr+1,(nr,oneTime,f):fs))
  (nr,_)<- readMVar eventListener
  return (nr-1)
\end{code}
]]></haskell>

Of course it is possible to remove handlers again:

<haskell><![CDATA[
\begin{code}
removeListener eventListener nr = do  
  (_,listener)<- readMVar eventListener
  modifyMVar_
    eventListener
    (\(nmr,fs) -> return (nmr,filter (\(n,_,_) -> not (n==nr)) fs))
\end{code}
]]></haskell>

<paragraph>
<title>Special handlers</title>
In this paragraph we define two general message handlers. 


The first one updates the global state. Furthermore it updates the view of the global state.
Since the handler functions are not evaluated in the gui thread we capsule every gui functionality within the call
of the gtk2hs standard function <tt>postGUIAsync</tt>.

<code><![CDATA[
\begin{code}
processMessage gui state send
  LAN_X_LOCO_INFO{locID=id,Z21.direction=dir,Z21.speed=sp,Z21.light=l}
   = do
       modifyMVar_ state$return.replaceNonActiveLoco
          (newLoco id)
          {speed=sp,direction=dir,light=l}
       return True
processMessage gui state send  LAN_X_BC_TRACK_POWER_ON = do
  postGUIAsync$toggleButtonSetActive (head$powerControl gui) True
  return True
processMessage gui state send  LAN_X_BC_TRACK_POWER_OFF = do
  postGUIAsync$toggleButtonSetActive (head$tail$powerControl gui) True
  return True
processMessage _ _ _ _ = return False
\end{code}
]]></code>

Another function that will be evaluated whenever a message is received is solely for logging purposes. 
It will display the message on the status label of the GUI. Furthermore the state of the current locomotive is printed to the console.

<haskell><![CDATA[
\begin{code}
logging gui state msg = do
  postGUIAsync guiStuff
  s<-readMVar state
  return True
   where
     guiStuff = do
       textViewSetEditable (statusLabel gui) False
       buffer<-textBufferNew Nothing
       textBufferInsertInteractiveAtCursor buffer (show msg) True
       textViewSetBuffer (statusLabel gui) buffer
\end{code}
]]></haskell>
</paragraph>
</paragraph>

</module>
