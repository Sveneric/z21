<?xml version="1.0" encoding="UTF-8"?>
<module>
<paragraph>
<title>Command language</title>

<code><![CDATA[
\begin{code}
module Z21.CommandLanguage where

import Z21.State
import Z21.Protocoll
import Z21.MessageEventListener

import Data.Maybe

import Text.Read

import Control.Concurrent.MVar
import Control.Concurrent.Timer
import Control.Concurrent.Suspend.Lifted

import Data.Word
import Control.Applicative
import Control.Monad


import Graphics.UI.Gtk

\end{code}
]]></code>
<paragraph>
<title>Scripting</title>
<haskell><![CDATA[
\begin{code}
data Kommando a 
      = Kom (MVar MessageListener -> MVar State ->  (Message -> IO Int) -> IO a)
       |Wait Int (Kommando a)
       |OnEvent Contact (Kommando a)
       |Sequenz (Kommando a) (Kommando a)

(<+>) c1@(Kom _) c2 = Sequenz c1 c2
(<+>) (Sequenz c1 c2) c3 = Sequenz c1 (c2<+>c3)
(<+>) (Wait sec c1) c2 = Wait sec (c1 <+> c2)
(<+>) (OnEvent contact c1) c2 = OnEvent contact (c1 <+> c2)


run listener state send (Kom c) = c listener state send
run listener state send (Sequenz c1 c2) = do
  run listener state send c1
  run listener state send c2
run listener state send (Wait sec c1) = do
  oneShotTimer (run listener state send c1) (sDelay$fromInteger$toInteger sec)
  return ()
run listener state send (OnEvent contact c1) = do
  addOneTimeListener listener eval
  return ()
  where
     eval (LAN_RMBUS_DATACHANGED (gi:gs)) = do
       let adder = if gi==0 then 0 else 10
       let numbered=filter (\(nr,entry)-> entry/=0)$zip [1..] gs
       if (null numbered) then return False else do
       let changedGroup = adder+fst(head numbered)
       let groupVal = fromInteger$toInteger$snd(head numbered)
       let addrs =  filter (\(nr,code)->code==groupVal)
                   $zip [(1::Int)..][1,2,4,8,16,32,64,128]
       if (null addrs)  then return False else do
       let newContact = (changedGroup,snd$head addrs)
       if (newContact /= contact) then return False else do
       run listener state send c1
       return True
     eval _ = return False

fahre richtung lokAdresse geschwindigkeit  = 
  Kom$ \eventListener state send -> do
    send (LAN_X_SET_LOCO_DRIVE lokAdresse 2 geschwindigkeit richtung)
    return ()

halte lokAdresse  = Kom $ \eventListener state send -> do
  s <- readMVar state
  let dir = getDirectionOfLoco lokAdresse s
  send (LAN_X_SET_LOCO_DRIVE lokAdresse 2 0 dir)
  return ()

warte sekunden = Wait sekunden 
wenn = OnEvent
endlos = foldr1 (<+>) . repeat
wiederhole n = foldr1 (<+>) . take n . repeat
macheNichts =   Kom$ \_ _ _ -> return ()

infixr 5 <+>

hinUndHer locoid hin her = 
      fahre Forward locoid 4
  <+> wenn hin (halte locoid)
  <+> warte 5 (fahre Backward locoid 4)
  <+> wenn her (halte locoid)
  <+> warte 5 macheNichts

pendel locoid hin her = endlos  (hinUndHer locoid hin her)


instance Functor Kommando where
  fmap f (Kom com) = Kom$ \ listener state send -> do
    result <- com  listener state send 
    return $ f result
  fmap f (Wait sec com) = Wait sec (fmap f com)
  fmap f (OnEvent contact com) = OnEvent contact (fmap f com)
  fmap f (Sequenz c1 c2) = (Sequenz (fmap f c1) (fmap f c2))

{--

instance Applicative Kommando where
  pure a = Kom$ \ listener state send -> return a
  (Kom fc) <*> (Kom cc) = Kom$ \ listener state send -> do
    f <- fc listener state send
    a <- cc listener state send
    return$ f a
  (Wait sec fc) <*> c2 = Wait sec (fc <*> c2)
  (OnEvent contact fc) <*> c2 = OnEvent contact (fc <*> c2)
--}
{--instance Monad Kommando where
  return a = Kom$ \ listener state send -> return a
  c1 >> c2 = 

--      eval (Wait sec c2) listener state send = c2 listener state send  
  (Wait sec c) >>=  fc = Wait sec (c >>= fc)
  (OnEvent contact c) >>=  fc = OnEvent contact (c >>= fc)
--}
{--
instance Monad Kommando where
  return x = Kom (\_ _ _ -> return x)
--  (>>) (Kom c1) (Kom c2) = Kom (\listener state send ->  c1 listener state send >> c2 listener state send)
  (>>=) (Kom c1) f = Kom (\listener state send ->  c1 listener state send >>= \x-> (let (Kom c2) = f x  in c2 listener state send))


type Command a =  IO a -> IO a
--}
\end{code}
]]></haskell>
</paragraph>
</paragraph>
</module>
