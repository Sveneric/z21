<?xml version="1.0" encoding="UTF-8"?>
<module>
<paragraph>
<title>Command language</title>
In this chapter we provide a tiny library, which enables the user to program automatic sequences an cycles.

<code><![CDATA[
\begin{code}
module Z21.CommandLanguage where

import Z21.Protocoll
import Z21.State
import Z21.MessageEventListener
import Z21.Commuting(Contact)

import Control.Concurrent.MVar
import Control.Concurrent.Timer
import Control.Concurrent.Suspend.Lifted

import Data.Bits
import Control.Applicative hiding ((<|>))
\end{code}
]]></code>
<paragraph>
<title>Scripting</title>
First we give a data definition for commands. The data type is a generic (polymorphic) type. It has a type variable. This is not used in any way. The only reason for this is, to make it an instance of the type class <tt>Monad</tt>. This will enable the use of the do-notation.
<haskell><![CDATA[
\begin{code}
data Command a  =
\end{code}
]]></haskell>
A simple command consists of a direct Z21 message.
<haskell><![CDATA[
\begin{code}
  Com Message
\end{code}
]]></haskell>
The next command is a timed command. After some seconds wait the command is to be executed.
<haskell><![CDATA[
\begin{code}
  |Wait Int (Command a)
\end{code}
]]></haskell>
Next a command is provided, which is triggered by some contact events. Only when every circuit track in the list of contacts has triggered a signal the command is excuted.
<haskell><![CDATA[
\begin{code}
  |OnEvent [Contact] (Command a)
\end{code}
]]></haskell>
A sequence of two commands will execute these command one after the other. It does not main, that we will wait for the first command to have finished completely.
<haskell><![CDATA[
\begin{code}
  |Sequenz (Command a) (Command a)
\end{code}
]]></haskell>
The last type of command allows to start to commands in parallel.
<haskell><![CDATA[
\begin{code}
  |Parallel (Command a) (Command a)
\end{code}
]]></haskell>


Furthermore we provide the possibility to call simple macros.
<haskell><![CDATA[
\begin{code}
  |Macro String
\end{code}
]]></haskell>
And it is possible to integrate an arbitrary IO action into an script. This will be used to ensure that connections can be closed and threads can be stoppped after the execution of a command. 
<haskell><![CDATA[
\begin{code}
  |IOCommand (IO a)
\end{code}
]]></haskell>


For simple debugging reasons an instance of the class <tt>Show</tt> is provided. We could not derive  the default implementation of this class, because <tt>IO</tt> is not an instance of <tt>Show</tt><footnote>I wonder why function types and types such as <tt>IO</tt> do not have an derived default instance of <tt>Show</tt>. This would make things easier in many situations.</footnote>. 

<haskell><![CDATA[
\begin{code}
instance Show (Command a) where
  show (Com message) = "("++show message++")"
  show (Wait secs message)
    = "(Wait "++show secs++" "++ show message++")"
  show (OnEvent cts command)
    = "(OnEvent "++show cts++" "++ show command++")"
  show (Sequenz c1 c2) = "("++show c1++"\n  <+> "++ show c2++")"
  show (Parallel c1 c2) = "("++show c1++"\n <|> "++ show c2++")"
  show (Macro name) = "(Macro "++name++")"
  show (IOCommand _) = "IOCommand"
\end{code}
]]></haskell>

The two constructors <tt>Sequenz</tt> and <tt>Parallel</tt> will not be used directly. Instead 
we define two operators to combine two commands. <ttt>&lt;+&gt;</ttt> is used for a sequential combination, <tt>&lt;|&gt;</tt> is used for a parallel combination.

<haskell><![CDATA[
\begin{code}
infixr 5 <+>
infixr 4 <|>
\end{code}
]]></haskell>
Most cases for the sequential operator a straight forward.
<haskell><![CDATA[
\begin{code}
(<+>) (Sequenz c1 c2) c3 = Sequenz c1 (c2<+>c3)
(<+>) (Wait sec c1) c2 = Wait sec (c1 <+> c2)
(<+>) (OnEvent contact c1) c2 = OnEvent contact (c1 <+> c2)
\end{code}
]]></haskell>
The interesting case is: where to put further commands after the parallel execution. We arbitrarily choose after the second command of two parallel commands. We will show examples on how this can be used to synchronize after several parralel executed commands.
<haskell><![CDATA[
\begin{code}
(<+>) (Parallel c1 c2) c3 = Parallel c1 (c2<+>c3)
(<+>) c1 c2 = Sequenz c1 c2
\end{code}
]]></haskell>
The parallel operator is a direct call to the constructor <tt>Parallel</tt>.
<haskell><![CDATA[
\begin{code}
(<|>) = Parallel
\end{code}
]]></haskell>
</paragraph>
<paragraph>
<title>Execution of commands</title>
The main function is called <tt>run</tt> and will execute a command. It gets the MessageListener, the global client state<footnote>This is not yet used.</footnote>, the function to send messages to the z21 control unit, a map for the marcos and eventually the command to be processed. 
<haskell><![CDATA[
\begin{code}
run :: MVar MessageListener
    -> MVar State
    -> (Message -> IO a)
    -> [(String, Command a1)]
    -> Command a1
    -> IO ()
\end{code}
]]></haskell>

Simple cases are: sending directly a message to the control unit and executing some IO action.

<haskell><![CDATA[
\begin{code}
run listener state send defs (Com c) = send c >> return ()
run listener state send defs (IOCommand c) = c >> return ()
\end{code}
]]></haskell>

Running a sequence of two commands can simply done by a sequence of recursive calls to <tt>run</tt> within a do-block.

<haskell><![CDATA[
\begin{code}
run listener state send defs (Sequenz c1 c2) = do
  run listener state send defs c1
  run listener state send defs c2
\end{code}
]]></haskell>

To wait some seconds before executing a command is implemented with the help of the function <tt>oneShotTimer</tt>. This will actually start a new thread.

<haskell><![CDATA[
\begin{code}
run listener state send defs (Wait sec c1) = do
  oneShotTimer 
   (run listener state send defs c1) 
   (sDelay$fromIntegral sec)
  return ()
\end{code}
]]></haskell>

The most complex command is the reaction to events on the circuit rails. This can be the point of synchronization. The command waits for a list of contacts. If these contacts are triggered by serveral commands in parallel, this is the moment where we synchronize on these command. 


The implementation resembles the implementation which we have done for commuting trains before. It is a generalization of the allready seen implementation. 

First of all we determine the number of contacts we are waiting for. The we define a local state variable for counting the contacts which were triggered. Then we add a new event listener function. 
<haskell><![CDATA[
\begin{code}
run listener state send defs (OnEvent contacts c1) = 
  let s = length contacts in do
  triggeredContacts <- newMVar ([])
  nr <- addOneTimeListener listener (evl triggeredContacts)
  return ()
\end{code}
]]></haskell>

The event listener function will react to <tt>LAN_RMBUS_DATACHANGED</tt> messages.  First of all it determines the contacts that are signified in the message to have triggered<footnote>To be done: Currently only messages for one single group are interpreted correctly.</footnote>

<haskell><![CDATA[
\begin{code}
  where
     evl triggeredContacts (LAN_RMBUS_DATACHANGED (gi:gs)) = do
       let adder = if gi==0 then 0 else 10
       let numbered=filter (\(nr,entry)-> entry/=0)$zip [1..] gs
       if (null numbered) then return False else do
       let changedGroup = adder+fst(head numbered)
       let groupVal = snd(head numbered)
       let addrs = filter (\(nr,code)->code==groupVal.&.code)
            $zip [(1::Int)..][0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80]
       if (null addrs)  then return False else do
       let newContacts = map (\(x,_)->(changedGroup,x))  addrs
\end{code}
]]></haskell>

Now we can check, if there are active contacts in the message, that we are waiting for and have not been triggered before. 

<haskell><![CDATA[
\begin{code}
       allreadyContacted <- readMVar triggeredContacts
       let actives = filter
                       (\c ->        c `elem` contacts 
                             && not (c `elem`allreadyContacted)) 
		       newContacts
       if null actives then return False else do
\end{code}
]]></haskell>

If there are new active contacts in the message we will add them to the list of allready triggered contacts. Afterwards we can check, if all contacts of the command have triggered.

<haskell><![CDATA[
\begin{code}
       modifyMVar_ triggeredContacts (\x-> return (x++actives))
       allreadyContacted <- readMVar triggeredContacts
       if (length allreadyContacted==length contacts)
         then run listener state send defs c1 >> return True
         else return False
     evl _ _ = return False
\end{code}
]]></haskell>

The command for parallel execution is simply executed by starting to new threads. For some unknown reason we do this by the help of <tt>oneShotTimer</tt> instead of <tt>forkIO</tt>.

<haskell><![CDATA[
\begin{code}
run listener state send defs (Parallel c1 c2) = do
  oneShotTimer (run listener state send defs c1) (sDelay 0)
  oneShotTimer (run listener state send defs c2) (sDelay 0)
  return ()
\end{code}
]]></haskell>

The call to a marco amounts in a lookup in the list of macros.

<haskell><![CDATA[
\begin{code}
run listener state send defs (Macro name) = 
  maybe
    (return ())
    (run listener state send defs)
    (lookup name defs) 
\end{code}
]]></haskell>
</paragraph>
<paragraph>
<title>Some useful commands (in German)</title>
In this section we provide some useful functions that construct commands. The names of the functions are given in the german language. English translations are given further down.


We start with straighforward function for driving a locomotive, switching turnouts and german names for constructors.
<haskell><![CDATA[
\begin{code}
fahre richtung lokAdresse geschwindigkeit  = 
  Com$LAN_X_SET_LOCO_DRIVE lokAdresse 2 geschwindigkeit richtung

halte lokAdresse = Com$LAN_X_SET_LOCO_DRIVE lokAdresse 2 0 Forward

schalteWeiche nr abzweig = do
  Com$LAN_X_SET_TURNOUT nr  True  abzweig
  warte 1$Com$LAN_X_SET_TURNOUT nr  False abzweig 

warte = Wait
wenn = OnEvent
\end{code}
]]></haskell>

Repeating commands can easily be implemented by means of a fold.

<haskell><![CDATA[
\begin{code}
endlos = foldr1 (<+>) . repeat

wiederhole n = foldr1 (<+>) . take n . repeat
\end{code}
]]></haskell>

A function is providing for doing nothing.

<haskell><![CDATA[
\begin{code}
macheNichts = Com$LAN_GET_SERIAL_NUMBER
\end{code}
]]></haskell>

The next two functions implement a Pendelautomatik.

<haskell><![CDATA[
\begin{code}
hinUndHer locoid hin her geschwindigkeit = do
  fahre Forward locoid geschwindigkeit
  wenn [hin] (halte locoid)
  warte 5 (fahre Backward locoid geschwindigkeit)
  wenn [her] (halte locoid)
  warte 5 macheNichts

pendel locoid hin her geschwindigkeit =
  endlos  (hinUndHer locoid hin her geschwindigkeit)
\end{code}
]]></haskell>

Further German names for constructors.

<haskell><![CDATA[
\begin{code}
vorwärts = Forward
rückwärts= Backward
\end{code}
]]></haskell>

<paragraph>
<title>English Translation</title>
Her the English versions of the functions above.
<haskell><![CDATA[
\begin{code}
go            = fahre
commute       = pendel
forthAndBack  = hinUndHer
doNothing     = macheNichts
switchTurnout = schalteWeiche
repeatNTimes  = wiederhole
wait          = warte
when          = wenn
forever       = endlos
stop          = halte
\end{code}
]]></haskell>
</paragraph>
<paragraph>
<title>Example Script</title>
We give a simple example script. It is written for the following track layout. There are 4 turnouts and 4 circuit tracks. Two locomotives. We want the locos cross over to the other track. The is done sequentually. Then both to go to the other end of the track. Now they have changed places from the original scenario. This is repeated so that eventually the starting point is reached for both locomotives.   

<bild name="trackLayout"/>

<haskell><![CDATA[
\begin{code}
crossing = 
      schalteWeg 1 4
  <+> warte 5 (fahre rückwärts 6 5)
  <+> wenn  [(1,4)] (halte 6)
  <+> schalteWeg 3 2
  <+> warte 5 (fahre rückwärts 7 5)
  <+> wenn [(1,2)] (halte 7)
  <+> schalteWeg 4 3
  <+> warte 5
   (     fahre vorwärts 6 5 <+> wenn [(1,3)] (halte 6)
    <|>  fahre vorwärts 7 5 <+> wenn [(1,1)] (halte 7)
    <|>  wenn  [(1,3),(1,1)]
      (schalteWeg 1 4
      <+> warte 5 (fahre rückwärts 7 5)
      <+> wenn [(1,4)] (halte 7)
      <+> schalteWeg 3 2
      <+> warte 5 (fahre rückwärts 6 5)
      <+> wenn [(1,2)] (halte 6)
      <+> schalteWeg 4 3
      <+> warte 5
        (      fahre vorwärts 6 5 <+> wenn [(1,1)] (halte 6)
          <|>  fahre vorwärts 7 5 <+> wenn [(1,3)] (halte 7)
          <|>  wenn [(1,3),(1,1)] macheNichts
        )
      )
   )   

schalteWeg 1 4 = do
  schalteWeiche 2 False
  schalteWeiche 4 True
  schalteWeiche 1 False
schalteWeg 4 1 = schalteWeg 1 4
schalteWeg 3 2 = do
  schalteWeiche 3 False
  schalteWeiche 4 False
  schalteWeiche 2 True
schalteWeg 2 3 = schalteWeg 3 2
schalteWeg _ _ = do
  schalteWeiche 1 True
  schalteWeiche 2 True
  schalteWeiche 3 True
  schalteWeiche 4 True
\end{code}
]]></haskell>
</paragraph>
</paragraph>
<paragraph>
<title>Making <tt>Command</tt> an instance of <tt>Monad</tt></title>
One of the nice thing of monads is the do-notation. The command language we presented seems to be a perfect candidate for an instance of <tt>Monad</tt>. Then we can use the do-notation as allready done in the example script in the section above.

However, the command language does not easily fit into a command. A type that is an instance of <tt>Monad</tt> needs to have a type variable. Our command language does not need a variable inner type. In the definition of <tt>Command</tt> we introduced a dummy type variable.
To make it an instance of <tt>Monad</tt> we first need it to be an instance of <tt>Functor</tt>. Actually we do not need this instance but it will be used pathologically.

<haskell><![CDATA[
\begin{code}
instance Functor Command where
  fmap f (Com com) = Com com
  fmap f (Wait sec com) = Wait sec (fmap f com)
  fmap f (OnEvent contact com) = OnEvent contact (fmap f com)
  fmap f (Sequenz c1 c2) = (Sequenz (fmap f c1) (fmap f c2))
  fmap f (Parallel c1 c2) = (Parallel (fmap f c1) (fmap f c2))
\end{code}
]]></haskell>

Since very recent times we need to be an instance of <tt>Applicative</tt>. We make a dummy implementation, which will never be used.


<haskell><![CDATA[
\begin{code}
bot = bot

instance Applicative Command where
  pure a = macheNichts
  f <*> a = fmap bot a 
\end{code}
]]></haskell>

Eventually we implement the instance of <tt>Monad</tt>. As a matter of fact we are onbly interested in the sequence operator <tt>&gt;&gt;</tt>. 

<haskell><![CDATA[
\begin{code}
instance Monad Command where
  return a = macheNichts
  c1 >> c2 = fmap (\_->bot)  c1 <+>  c2
  c1 >>= fc2 = fmap (\_->bot)  c1 <+>  fc2 bot 
\end{code}
]]></haskell>
</paragraph>
<input>CommandParser.tex</input>
</paragraph>


</module>
