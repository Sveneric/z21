<?xml version="1.0" encoding="UTF-8"?>
<module>

<paragraph>
<title>The Z21 Protocoll</title>
This module implements communication with the Z21 server via User Datagram Protocol (UDP). The protocoll for communication with the Z21 server is defined in <cite ref="z21prot"/>. This module represents the transport layer of our application.

For communication we need the Haskell <tt>ByteString</tt> module. We need unsigned words as provided by the module <tt>Data.Word</tt> and do some bit manipulations as provided by the module <tt>Data.Bits</tt>.  Therefore the following imports are done.
<haskell><![CDATA[
\begin{code}
module Z21.Protocoll where

import qualified Data.ByteString as BS
import Network.Socket.ByteString

import Data.Bits
import Data.Word
\end{code}
]]></haskell>


<paragraph>
<title>Messages</title>
First of all we define a data type for the Z21 messages. Every message is represented by a constructor. Some messages have further information like the address of a locomotive or the speed for a locomotive. Some messages have complex information. In these cases the record syntax with named fields is used.
<haskell><![CDATA[
\begin{code}
data Message 
 = LAN_GET_SERIAL_NUMBER
  |LOGOFF
  |LAN_X_GET_VERSION
  |LAN_X_GET_STATUS
  |LAN_X_SET_TRACK_POWER_OFF
  |LAN_X_SET_TRACK_POWER_ON
  |LAN_X_BC_TRACK_POWER_ON
  |LAN_X_BC_TRACK_POWER_OFF
  |LAN_X_SET_STOP
  |LAN_X_GET_LOCO_INFO Int
  |LAN_X_SET_LOCO_DRIVE
     {locID::Int, steps::Word8, speed::Word8, direction::Direction}
  |LAN_X_SET_LOCO_FUNCTION
     {locID::Int, switch::FunctionSwitch, index::Word8}
  |LAN_X_GET_FIRMWARE_VERSION
  |LAN_GET_BROADCASTFLAGS
  |LAN_SET_BROADCASTFLAGS{ general::Bool,rbus::Bool,systemState::Bool}
  |LAN_GET_LOCOMODE Int
  |LAN_SET_LOCOMODE {locId:: Int, mode:: LOC_MODE}
  |LAN_GET_HWINFO
  |LAN_X_LOCO_INFO
   {locID::Int
   ,busy::Bool
   ,stpes::SpeedSteps
   ,direction::Direction
   ,speed::Word8
   ,doubleTraction::Bool
   ,smartSearch::Bool
   ,light::Bool
   ,f1::Bool
   ,f2::Bool
   ,f3::Bool
   ,f4::Bool
   ,f5::Bool
   ,f6::Bool
   ,f7::Bool
   ,f8::Bool
   ,f9::Bool
   ,f10::Bool
   ,f11::Bool
   ,f12::Bool
   ,f13::Bool
   ,f14::Bool
   ,f15::Bool
   ,f16::Bool
   ,f17::Bool
   ,f18::Bool
   ,f19::Bool
   ,f20::Bool
   }
  |LAN_RMBUS_GETDATA Word8
  |LAN_RMBUS_DATACHANGED [Word8]
  |SERIAL_NUMBER
  |LAN_X_SET_TURNOUT Word16 Bool Bool
  |LAN_X_GET_TURNOUT_INFO Word16
  |LAN_X_TURNOUT_INFO  Word16 Word8
  |LAN_X_UNKNOWN_COMMAND 
  |UNKNOWN [Word8]
     deriving (Show,Read)
\end{code}
]]></haskell>

Some more data types are used within this definition. First of all a simple enum type for the direction of a locomotive:
<haskell><![CDATA[
\begin{code}
data Direction = Forward | Backward deriving (Eq,Show,Read,Enum)

switchDirection Forward = Backward
switchDirection Backward = Forward

isForward  = (==)Forward 
\end{code}
]]></haskell>

We need a type to denote the protocoll of a locomotives decoder. Currently two different decoder formats are known. The standard dcc format and motorolas mm format.

<haskell><![CDATA[
\begin{code}
data LOC_MODE = DCC|MM  deriving (Show,Eq,Read)
isDCC = (==)DCC
\end{code}
]]></haskell>

For switches (e.g. light) there are three commands. Turning it off or on and simple switching it.

<haskell><![CDATA[
\begin{code}
data FunctionSwitch = On|Off|Switch deriving (Show,Eq,Read)

functionSwitchCode:: FunctionSwitch -> Word8
functionSwitchCode Off = 0x00
functionSwitchCode On = 0x40
functionSwitchCode Switch = 0x80
\end{code}
]]></haskell>

Finally there are three different types for decoder speed selection: 14, 28 and 128 steps.

<haskell><![CDATA[
\begin{code}
data SpeedSteps = S14|S28|S128 deriving (Show,Eq,Read)
\end{code}
]]></haskell>

Currently the implementation does not realy bother about these three different types. We alway assume 128 steps.

</paragraph>
<paragraph>
<title>Sending</title>
This paragraph contains the implementation of function <tt>mkmessage</tt> for serializing Z21 messages 
as a byte string in order to send them as UDP message to the z21 server. For each message a list of 8 bit words is created. 
The function <tt>pack</tt> from module <tt>Data.ByteString</tt> is applied to create the byte string.

We use the hexadecimal notation for the 8 bit words directly from the specification of the protocoll<cite ref="z21prot"/>.

<haskell><![CDATA[
\begin{code}
mkMessage LAN_GET_SERIAL_NUMBER = BS.pack [0x04,0x00,0x10,0x00]
mkMessage LAN_GET_HWINFO =  BS.pack [0x04,0x00, 0x1A, 0x00]
mkMessage LOGOFF = BS.pack [0x08,0x00,0x10,0x00]
mkMessage LAN_X_GET_VERSION
  = BS.pack [0x07,0x00,0x40,0x00,0x21,0x21,0x00]
mkMessage LAN_X_GET_STATUS
  =  BS.pack [0x07,0x00,0x40,0x00,0x21,0x24,0x05]
mkMessage LAN_X_SET_TRACK_POWER_OFF
 = BS.pack [0x07,0x00,0x40,0x00,0x21,0x80,0xA1]
mkMessage LAN_X_SET_TRACK_POWER_ON
 = BS.pack [0x07,0x00,0x40,0x00,0x21,0x81,0xA0]
mkMessage LAN_X_SET_STOP =  BS.pack [0x06,0x00,0x40,0x00,0x80,0x80]
mkMessage (LAN_X_GET_LOCO_INFO locID)
  = BS.pack [0x09,0x00,0x40,0x00,xheader,db0,db1,db2,xorbyte]
 where
   xheader = 0xE3
   db0 = 0xF0
   [db1,db2] =  mkdLocIDBytes locID
   xorbyte = xheader `xor` db0 `xor` db1 `xor` db2 
mkMessage LAN_X_SET_LOCO_DRIVE
            {locID=locID, steps=st, speed=sp, direction=dir}
 = msg_LAN_X_SET_LOCO_DRIVE locID st sp dir
mkMessage LAN_X_SET_LOCO_FUNCTION
            {locID=locID, switch=switch, index=index}
 = msg_LAN_X_SET_LOCO_FUNCTION locID switch index 
mkMessage LAN_X_GET_FIRMWARE_VERSION
  = BS.pack [0x07,0x00,0x40,0x00,0xF1,0x0A, 0xFB]
mkMessage LAN_GET_BROADCASTFLAGS = BS.pack [0x04,0x00, 0x51, 0x00]
--Vorsicht. Hier ist die Dokumentation sehr irreführend
-- jaja little endian. Dann schreibt das auch so auf!
mkMessage LAN_SET_BROADCASTFLAGS
            {general=general,rbus=rbus,systemState=systemState}
 = BS.pack ([0x08,0x00, 0x50, 0x00, byte,sysByte,0x00,0x00])
  where
    genByte = if general then 0x01::Word8 else 0
    rbusByte = if rbus then 0x02::Word8 else 0
    sysByte = if systemState then 0x01::Word8 else 0
    byte = genByte .|. rbusByte     
mkMessage (LAN_GET_LOCOMODE locID)
  = BS.pack ([0x06,0x00, 0x60, 0x00]++mkdLocIDBytes locID)
mkMessage LAN_SET_LOCOMODE {locId=id, mode=md}
 = msg_LAN_SET_LOCOMODE id md
mkMessage (LAN_RMBUS_DATACHANGED bs) 
  = BS.pack ([0x0F,0x00,0x80,0x00]++bs)
mkMessage (LAN_RMBUS_GETDATA b) = BS.pack [0x05,0x00, 0x81,0x00,b]
mkMessage (LAN_X_SET_TURNOUT address1 active first)
  = BS.pack [0x09,0x00,0x40,0x00,xheader,db0,db1,db2,xorbyte]
 where
   address = address1+3
   xheader = 0x53
   db0 = fromIntegral$shiftR address 8
   db1 = fromIntegral address
   db2 = (0x80::Word8)
         .|. (if active then (0x08::Word8) else 0x00)
         .|. (if first then (0x01::Word8) else 0x00)
   xorbyte =  xheader `xor` db0 `xor` db1 `xor` db2
mkMessage (LAN_X_GET_TURNOUT_INFO address1)
  = BS.pack [0x08,0x00,0x40,0x00,0x43,db0,db1,0x43`xor`db0`xor`db1]
 where
   address = address1+3
   db0 = fromIntegral$shiftR address 8
   db1 = fromIntegral address 
mkMessage (LAN_X_TURNOUT_INFO address value)
  = BS.pack [0x09,0x00,0x40,0x00,0x43,db0,db1,db2,xorbyte]            
 where
   db0 = fromIntegral$shiftR address 8
   db1 = fromIntegral address 
   db2 = value
   xorbyte = 0x43`xor`db0`xor`db1`xor`db2
mkMessage (UNKNOWN _) = BS.pack [0x40,0x61,0x82]
\end{code}
]]></haskell>

Thus messages can be send as UDP to the server.

<haskell><![CDATA[
\begin{code}
sendMsg socket addr msg = do
  putStrLn ("> "++show msg)
  sendTo socket (mkMessage msg) addr
\end{code}
]]></haskell>
</paragraph>
<paragraph>
<title>Receiving</title>
When receiving a byte string we will read it as a Z21 message. Thus we write the inverse function <tt>readMessage</tt>.
The following equation should hold: \[id =  \mbox{readMessage} \circ \mbox{mkMessage}\]

The given implementation uses pattern matching on the list of bytes.<footnote>I am sure there is a much smarter way to implement this. The list of bytes coding a message is written in both functions <tt>mkMessage</tt> and <tt>readMessage</tt>. This seems to be error prone.</footnote>
  

<haskell><![CDATA[
\begin{code}
readMessage = rM.BS.unpack 
  where 
   rM :: [Word8] -> Message
   rM [0x04,0x00,0x10,0x00] = LAN_GET_SERIAL_NUMBER
   rM [0x04,0x00, 0x1A, 0x00] = LAN_GET_HWINFO
   rM [0x08,0x00,0x10,0x00] = LOGOFF
   rM [0x07,0x00,0x40,0x00,0x21,0x21,0x00] = LAN_X_GET_VERSION 
   rM [0x07,0x00,0x40,0x00,0x21,0x24,0x05] = LAN_X_GET_STATUS
   rM [0x07,0x00,0x40,0x00,0x21,0x80,0xA1] = LAN_X_SET_TRACK_POWER_OFF
   rM [0x07,0x00,0x40,0x00,0x21,0x81,0xA0] = LAN_X_SET_TRACK_POWER_ON
   rM [0x07,0x00,0x40,0x00,0x61,0x01,0x60] = LAN_X_SET_TRACK_POWER_ON
   rM [0x07,0x00,0x40,0x00,0x61,0x00,0x61] = LAN_X_SET_TRACK_POWER_OFF
   rM [0x06,0x00,0x40,0x00,0x80,0x80] = LAN_X_SET_STOP
   rM [0x09,0x00,0x40,0x00,0xE3,0xF0,0xE3,db1,db2,xorbyte] 
      = LAN_X_GET_LOCO_INFO (mkLocoInfo db1 db2)
   rM [0x0A,0x00,0x40,0x00,0xE4,0xF8,db1,db2,db3,xorbyte]
     = LAN_X_SET_LOCO_FUNCTION 
         { locID = (mkLocoInfo db1 db2)
         , switch = if (db3 .&. 0xC0 == 0) then Off
                    else if (db3 .&. 0xC0 == 0xC0) then On
                         else Switch
         , index = db3 .&. 0x3F}
   rM [0x0A,0x00,0x40,0x00,0xE4,db0,db1,db2,db3,xorbyte]
     = LAN_X_SET_LOCO_DRIVE 
         { locID = (mkLocoInfo db1 db2)
         , steps = db0 `mod` 16
         , speed = db3 `mod`128
         , direction = if db3 >= 128 then Forward else Backward}
   rM [0x07,0x00,0x40,0x00,0xF1,0x0A, 0xFB]
     = LAN_X_GET_FIRMWARE_VERSION
   rM [0x04,0x00, 0x51, 0x00] = LAN_GET_BROADCASTFLAGS
   rM [0x06,0x00, 0x60, 0x00,hi,lo]
     = LAN_GET_LOCOMODE  (  (fromIntegral hi)*256 + (fromIntegral lo))
   rM [0x07,0x00,0x61,0x00,locIDH,locIDL,mode] 
     = LAN_SET_LOCOMODE 
         { locId =   (fromIntegral locIDH)*256
                   + (fromIntegral locIDL)
         , mode = if mode==0 then DCC else MM} 
   rM (l:0x00:0x40:0x00:0xEF:db0:db1:db2:db3:db4:db5:db6:_) 
     = LAN_X_LOCO_INFO
         { locID = (mkLocoInfo db0 db1)
         , busy = db2 .&. 0x08 == 0x08
         , stpes
            = let step = db2 .&. 0x07
              in if step == 0 then S14
                 else if step == 1 then S28
                      else S128 
         , direction = if db3 .&. 0x80 == 0x80
                       then Forward else Backward
         , speed =  db3 .&. 0x7F
         , doubleTraction = db4 .&. 0x40 == 0x40
         , smartSearch = db4 .&. 0x20 == 0x20
         , light  = db4 .&. 0x10 == 0x10
         , f1 = db4 .&. 0x01 == 0x01
         , f2 = db4 .&. 0x02 == 0x02
         , f3  = db4 .&. 0x04 == 0x04
         , f4 = db4 .&. 0x08 == 0x08
         , f5 = db5 .&. 0x01 == 0x01
         , f6 = db5 .&. 0x02 == 0x02
         , f7 = db5 .&. 0x04 == 0x04
         , f8 = db5 .&. 0x08 == 0x08
         , f9 = db5 .&. 0x10 == 0x10
         , f10 = db5 .&. 0x20 == 0x20
         , f11 = db5 .&. 0x40 == 0x40
         , f12 = db5 .&. 0x80 == 0x80
         , f13 = db6 .&. 0x01 == 0x01
         , f14 = db6 .&. 0x02 == 0x02
         , f15 = db6 .&. 0x04 == 0x04
         , f16 = db6 .&. 0x08 == 0x08
         , f17 = db6 .&. 0x10 == 0x10
         , f18 = db6 .&. 0x20 == 0x20
         , f19 = db6 .&. 0x40 == 0x40
         , f20 = db6 .&. 0x80 == 0x80         
         }
   rM (0x0F:0x00:0x80:0x00:bytes) = LAN_RMBUS_DATACHANGED bytes
   rM (0x08:0x00:0x10:0x00:serialNumber) = SERIAL_NUMBER
   rM (0x09:0x00:0x40:0x00:0x43:db0:db1:db2:_:[])
     = LAN_X_TURNOUT_INFO ((mkLocoInfo db0 db1)-3) db2
   rM (0x0F:0x00:0x40:0x00:0x61:0x82:0xE3:[]) = LAN_X_UNKNOWN_COMMAND 
   rM bytes = UNKNOWN  bytes

   mkLocoInfo db1 db2
     =   (fromIntegral (db1 .&. 0x3F)) * 2^8
       + (fromIntegral db2)
\end{code}
]]></haskell>

Some of the more complicated messages are done in seperate functions. 


The message for setting a locomotive system mode.
<haskell><![CDATA[
\begin{code}
msg_LAN_SET_LOCOMODE:: Int -> LOC_MODE -> BS.ByteString
msg_LAN_SET_LOCOMODE locID locMode
  = BS.pack [0x07,0x00, 0x61,0x00
            ,fromIntegral (locID `div` 256)
            ,fromIntegral (locID `mod` 256)
            ,if isDCC locMode then 0 else 1]
\end{code}
]]></haskell>

The message for getting a locomotive to drive in a certain direction by a certain speed.
<haskell><![CDATA[
\begin{code}
msg_LAN_X_SET_LOCO_DRIVE locID steps speed direction
  = BS.pack [0x0A,0x00,0x40,0x00,xheader,db0,db1,db2,db3,xorbyte]
    where
      xheader = 0xE4
      db0 =  16+steps
      [db1,db2] =  mkdLocIDBytes locID
      db3 =  (if isForward direction then 128 else 0)+speed 
      xorbyte = xheader `xor` db0 `xor` db1 `xor` db2 `xor` db3
\end{code}
]]></haskell>

The message for switching a locomotive's function.

<haskell><![CDATA[
\begin{code}
msg_LAN_X_SET_LOCO_FUNCTION locID switch index
  = BS.pack [0x0A,0x00,0x40,0x00,xheader,db0,db1,db2,db3,xorbyte]
    where
      xheader = 0xE4
      db0 =  0xF8
      [db1,db2] =  mkdLocIDBytes locID
      db3 =  functionSwitchCode switch + index
      xorbyte = xheader `xor` db0 `xor` db1 `xor` db2 `xor` db3
\end{code}
]]></haskell>

The decoder address of a locomotive is decoded in two bytes in the following way.

<haskell><![CDATA[
\begin{code}
mkdLocIDBytes:: Int -> [Word8]
mkdLocIDBytes locID = 
  [fromIntegral (locID `div` 256 `mod` (128+64))
  ,fromIntegral (locID `mod` 256)]
\end{code}
]]></haskell>
</paragraph>
</paragraph>
</module>
 
