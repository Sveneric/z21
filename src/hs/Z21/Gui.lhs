<?xml version="1.0" encoding="UTF-8"?>
<module>
<paragraph>
<title>Graphical user interface</title>
The module <tt>Gui</tt> contains the definition of the graphical user interface of the application. As GUI-library the gtk2hs-library is 
used. It is a direct Haskell api for the <em>Gtk+</em> library. A basic tutorial can be found in <cite ref="gtk2hstutorial"/>.


Some useful hints on gtk2hs and threads can be found on the internet in an article by Daniel Wagner<cite ref="threadgtk2hs"/>.
<code><![CDATA[
\begin{code}
module Z21.Gui where
import Z21.Protocoll hiding(light,direction, speed)
import Z21.State
import Util

import Data.Word

import Control.Concurrent

import Graphics.UI.Gtk
\end{code}
]]></code>


<paragraph>
<title>Controls</title>
A data type is defined, which contains all the gui controls of the client application. These controls may change their values due to modifications of the global state.

<code><![CDATA[
\begin{code}
data GuiControls a
 = GuiControls
   { mainBox          :: VBox
   , statusLabel      :: TextView
   , addrLabel        :: Label
   , speedAdjust      :: Adjustment
   , addrSelect       :: [Button]
   , lightButton      :: ToggleButton
   , directionControl :: [RadioButton]
   , powerControl     :: [RadioButton]
   }
\end{code}
]]></code>

<paragraph>
<title>Construction</title>
We define a straighforward constructor function for the gui controls.

<code><![CDATA[
\begin{code}
newGuiControls = do
  mainPanel    <- vBoxNew False 10
  statusLabel  <- textViewNew
  addrLabel    <- labelNew$Just$show 1 
  speedAdjust  <- adjustmentNew 0.0 0.0 128.0 1 1.0 1.0
  lightButton  <- toggleButtonNewWithLabel "Light On/Off"
  forwardButton<- radioButtonNewWithLabel$show Forward 
  backButton   <-
    radioButtonNewWithLabelFromWidget forwardButton $show Backward
  addrButtons  <- sequence $map (buttonNewWithLabel.show) (201:[1 .. 20 ])
  onButton     <- radioButtonNewWithLabel "Power On"
  offButton    <-
    radioButtonNewWithLabelFromWidget onButton "Power Off"
  textViewSetWrapMode statusLabel WrapChar
  widgetSetSizeRequest statusLabel (-1) 180
  return 
    GuiControls
      { mainBox          = mainPanel
      , statusLabel      = statusLabel
      , addrLabel        = addrLabel
      , speedAdjust      = speedAdjust
      , addrSelect       = addrButtons
      , lightButton      = lightButton  
      , directionControl = [forwardButton,backButton]
      , powerControl     = [onButton,offButton]
      }
\end{code}
]]></code>
</paragraph>
</paragraph>
<paragraph>
<title>Updates</title>
When in the global state a new current locomotive is set, the gui controls need to be updated. this can be achieved by use of the following function.

<code><![CDATA[
\begin{code}
updateGUI Loco{lid=lid,speed=sp,direction=dir,light=li} gui = do
  if (Forward==dir)
    then toggleButtonSetActive (head$directionControl gui) True
    else toggleButtonSetActive (head$tail$directionControl gui) True    
  adjustmentSetValue (speedAdjust gui)$fromInteger$toInteger sp
  toggleButtonSetActive (lightButton gui) li
  labelSetText (addrLabel gui) (show lid)
\end{code}
]]></code>
</paragraph>

<paragraph>
<title>Layout</title>
This section the controls of the application GUI and put them together with some layout. Propably it would have been better to use the GUI builder tool glade in the first place rather than doing everything manually. However, here we go.

The first function is an auxilary function to create the layout for a list of radio buttons:
<code><![CDATA[
\begin{code}
mkLayoutRadioButtons (b1:bs) = do
  mainbox <- vBoxNew False 0
  box1    <- hBoxNew False 0
  box2    <- hBoxNew False 10
  containerSetBorderWidth box2 10
  boxPackStart box1 box2 PackNatural 0  
  boxPackStart box2 b1 PackNatural 0
  sequence$map (\b -> boxPackStart box2 b PackNatural 0) bs
  boxPackStart mainbox box1 PackNatural 0
  return mainbox
\end{code}
]]></code>

The next function creates a layout for the address selection buttons of the clients gui. They are placed in rows of three.

<code><![CDATA[
\begin{code}
mkLayoutAddrSelect gui = do
  let buttons = addrSelect gui
  let adj = speedAdjust gui
  lines  <-sequence$take (length buttons `div` 3)$repeat hButtonBoxNew
  sequence_ 
      $ map (\(bb,bs)-> set bb [ containerChild := b| b <- bs ])
      $ zip lines
      $ splitNChunks (length buttons `div` length lines) buttons
  vbox <- vButtonBoxNew
  set vbox [ containerChild := l| l <- lines ]
  return  vbox
\end{code}
]]></code>


The following function creates a layout  for the speed control, light switch, direction and the address selection.

<code><![CDATA[
\begin{code}
mkLayoutLocoGui gui = do
  let adj1 = speedAdjust gui
  forwardBackwardButton <- mkLayoutRadioButtons (directionControl gui)

  let lightB = lightButton gui

  box1 <- hBoxNew False 0
  vsc  <- vScaleNew adj1
  box2 <- vBoxNew False 0
  boxPackStart box1 box2 PackGrow 0

  hsc1 <- hScaleNew adj1
  boxPackStart box2 hsc1 PackGrow 0

  mainBox <- vBoxNew False 10  

  addrLBox <- hBoxNew False 10  
  lab <- labelNew$Just "Current Loco Address: "
  boxPackStart addrLBox lab PackGrow 0
  boxPackStart addrLBox (addrLabel gui) PackGrow 0
  boxPackStart mainBox addrLBox PackGrow 0

  boxPackStart mainBox forwardBackwardButton PackGrow 0
  boxPackStart mainBox lightB PackGrow 0
  
  lSel <- labelNew$Just "Speed"
  boxPackStart mainBox lSel PackGrow 0
  boxPackStart mainBox box1 PackGrow 0

  lSel <- labelNew$Just "Loco Address Selection"
  boxPackStart mainBox lSel PackGrow 0
  addrSelect <- mkLayoutAddrSelect gui
  boxPackStart mainBox addrSelect PackGrow 0

  return mainBox
\end{code}
]]></code>

Putting everything together and adding the power control and the status display to the layout:


<code><![CDATA[
\begin{code}
createOverallLayout gui = do
  let mainb = mainBox gui

  let onOffControl = powerControl gui 
  onOffButtons <- mkLayoutRadioButtons onOffControl

  statusBox <- scrolledWindowNew Nothing Nothing 
  widgetSetSizeRequest statusBox (-1) 80
  scrolledWindowAddWithViewport statusBox (statusLabel gui)

  boxPackStart mainb onOffButtons PackGrow 0

  locoPanel <- mkLayoutLocoGui gui
  boxPackStart mainb locoPanel PackGrow 0
  lSel <- labelNew$Just "Received Messages"
  boxPackStart mainb lSel PackGrow 0
  boxPackStart mainb statusBox PackGrow 0
\end{code}
]]></code>



</paragraph>

<paragraph>
<title>Event Handler</title>
In this section we will add event handlers to the controls of the GUI.


The first function is a utility function, that adds event handlers to a list of pairs of buttons and events.
<code><![CDATA[
\begin{code}
addEventsRadioButtons las@((b1,_):_) = do
  toggleButtonSetActive b1 True
  sequence_$map (\(b,a)-> onToggled b (a >> return ())) las
\end{code}
]]></code>
Events for the buttons in the address selection control:
<code><![CDATA[
\begin{code}
addEventAddrSelect gui state sendF = do
  let adj = speedAdjust gui
  sequence$map
   (\b -> 
     onClicked b$ do
      nrl <- buttonGetLabel b
      let nr = read nrl 
      sendF $LAN_X_GET_LOCO_INFO nr 
      modifyMVar_ state (return.selectLoco nr)
      s <- readMVar state
      let loco = currentLoco s
      updateGUI loco gui
   )
   (addrSelect gui)
\end{code}
]]></code>

Events for further controls. First the event for the direction switch:

<code><![CDATA[
\begin{code}
addGuiEvents gui state send =  do
  let adj1 = speedAdjust gui  
  addEventsRadioButtons
      $map (\x->(x, do
            label <- buttonGetLabel x
            let dir = (read label)::Direction 
            modifyMVar_ state  (return.setDirection dir)
            s <- readMVar state
            let loco = currentLoco s
            adjustmentSetValue adj1 0
            send  $LAN_X_SET_LOCO_DRIVE (address loco) 2 0 dir
           ))
           (directionControl gui)
\end{code}
]]></code>
Event for the light switch:
<code><![CDATA[
\begin{code}
  let lightB = lightButton gui  
  onClicked lightB $ do  
    s <- readMVar state
    let loco = currentLoco s
    activ <- toggleButtonGetActive lightB
    let x = if activ then On else Off
    modifyMVar_ state  (return.setLight activ)    
    send LAN_X_SET_LOCO_FUNCTION{locID=address loco,switch=x,index=0}
    return ()
\end{code}
]]></code>
Event for speed adjustment:
<code><![CDATA[
\begin{code}
  onValueChanged adj1 $ do 
    s <- readMVar state
    let loco = currentLoco s
    val <- adjustmentGetValue adj1
    let v = (truncate val)::Word8
    modifyMVar_ state  (return.setSpeed v)    
    send$LAN_X_SET_LOCO_DRIVE
          (address loco) 2 v (Z21.State.direction loco)
    return ()
\end{code}
]]></code>
The event for the power switch:
<code><![CDATA[
\begin{code}
  addEventsRadioButtons$ 
     zip (powerControl gui) 
       [send LAN_X_SET_TRACK_POWER_ON
       ,send LAN_X_SET_TRACK_POWER_OFF]  
\end{code}
]]></code>

<code><![CDATA[
\begin{code}
  addEventAddrSelect gui state send
\end{code}
]]></code>


</paragraph>

<paragraph>
<title>Overall Window Creation</title>
Eventually we provide a function to build everything, add events and display it in a window.
<code><![CDATA[
\begin{code}
createGuiWindow gui state send = do
  -- Create a new window
  window <- windowNew
  
  -- Sets the border width of the window.
  set window [ containerBorderWidth := 10 ]

  createOverallLayout gui

  -- sets the contents of the window
  set window [ containerChild := mainBox gui ]
  
  addGuiEvents gui state send

  return window
\end{code}
]]></code>
</paragraph>


</paragraph>
</module>
