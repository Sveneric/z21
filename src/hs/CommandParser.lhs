<?xml version="1.0" encoding="UTF-8"?>
<module>
<paragraph>
<paragraph>
<title>Command language parser</title>
In this section we present a parser for the command language. Writing parsers has been a killer application for lazy evaluated functional languages. The idea is to write higher order combinator functions which combine two parsers sequentially and alternatively. The sequence means: first parse the input with one parser and then parse the remaining tokens with a second parser. The combination results in a new parser. The alternative means: try to parse the input wiht one of the two parsers. A fine example of a cominator parser is given in <cite ref="frostlaunch:88"/>. This work has even be done in pre Haskell times. With the arrival of monads and the do-notation <cite ref="Launchbury93lazyimperative"/><cite ref="Launchbury93lazyimperative"/>  it became clear that the sequence operator can be written as the bind operation in monads. Thus parser combinators where implemented as instance of <tt>Monad</tt>. Therefore parser combinator libraries are now called monadic parser libraries. A mature and efficient Haskell parser library is <tt>parsec</tt><cite ref="leijen2001"/>. Within this section a parser is implemented with the parsec library.
<code><![CDATA[
\begin{code}
module Main where
import Z21.CommandLanguage hiding ((<|>))
import qualified Z21.CommandLanguage((<|>))

import Z21.Protocoll
import Z21.Constants

import Text.ParserCombinators.Parsec
import Text.Parsec.Pos
import Text.Parsec.Prim

import Language.Haskell.Lexer

import Z21.State
import Z21.MessageEventListener

-- Network library
import Network.Socket hiding (send, sendTo, recv, recvFrom)

--concurrency
import Control.Monad (forever)
import Control.Concurrent
import Control.Concurrent.Timer
import Control.Concurrent.Suspend.Lifted

--prog args and such
import System.Environment
\end{code}
]]></code>


<paragraph>
<title>Lexer and Parsing of Token</title>
 Before we write the actual parser we need a lexer. Since our command language is defined in Haskell, we can use a Haskell lexer. 
Fortunately a complete Haskell lexer is available in the module <tt>Language.Haskell.Lexer</tt>. We will apply this lexer by the call of <tt>lexerPass0</tt>, afterwards remove white space token and eventually add the position information as needed by the parsec library.
<code><![CDATA[
\begin{code}
lexAll fileName
  = (map (\(tok,(pos,s))->
     (newPos fileName (fst$simpPos pos) (snd$simpPos pos),(tok,s))))
    . rmSpace . lexerPass0
\end{code}
]]></code>


The Haskell lexer emits token consisting of a pair. The first component is an enumeration type signifiying the token type. The second component is the actual token string.

<code><![CDATA[
\begin{code}
type HaskellTok = (Token, String)
\end{code}
]]></code>

We define two atomic parser. One for accepting integer token and for accepting arbitrary identifier token.

<code><![CDATA[
\begin{code}
integerNumber ::  (GenParser (SourcePos,HaskellTok) () Int)
integerNumber = token (show.snd) fst (testIdent.snd)
  where 
    testIdent v@(IntLit,n) = Just$read n 
    testIdent _ = Nothing

identifier = token (show.snd) fst (testIdent.snd)
  where 
    testIdent v@(Varid,n) = Just n
    testIdent _ = Nothing
\end{code}
]]></code>

We provide a general function for generating arbritrary atomic parsers. 

<code><![CDATA[
\begin{code}
parseTok
  :: Token -> String -> a -> (GenParser (SourcePos,HaskellTok) () a)
parseTok tok name res = token showToken posToken testToken
  where 
    showToken (pos, tok) = show tok
    posToken  (pos,_)    = pos

    testToken (pos, v@(t,n))
      |t == tok && n == name  = Just res 
      |otherwise              = Nothing
\end{code}
]]></code>

Thus we can define some specific parsers.

<code><![CDATA[
\begin{code}
ident name res = parseTok Varid name res

constructor con res = parseTok Conid con res

special name  =  parseTok Special name ()

reservedop name  = parseTok Reservedop name ()

pVarsym name = parseTok Varsym name ()
\end{code}
]]></code>
</paragraph>
<paragraph>
<title>Grammar</title>
Now we can write down a grammar for the command language. Sequences can be expressed with the do notation. Alternatives are written down with the combinator <tt>Text.Parsec.Prim.&lt;|&gt;</tt>. However the combinator <tt>&lt;|&gt;</tt> for effeciency reasons does not perform backtracking. If the first parser can be successfully applied, the second parser will not be considered any more.  


We write the grmmar top down and start with the start rule: <tt>pSkript</tt>. A script will consist of a number of macro definitions followed by one single command.  


A skript for the Z21 client consists of several macro definitions. These are followed by a single command, which is to be executed. Since both, a macro definition and a command can start with an indentifier, we need to apply backtrakcing. This can be done with the <tt>parsec</tt> function <tt>try</tt>.
<code><![CDATA[
\begin{code}
pSkript =
  do
   defs <- many (Text.ParserCombinators.Parsec.try pDefinition)
   cmd  <- pCommand
   return (defs,cmd)
\end{code}
]]></code>

A macro definition consists of some arbitrary identifier followed by the reserved operator symbol <tt>=</tt> and finally the command for the macro.

<code><![CDATA[
\begin{code}
pDefinition = do
  n <- identifier
  reservedop "="
  cmd <- pCommand
  return (n,cmd)
\end{code}
]]></code>

The entry rule for commands is the parallel operation on commands. 

<code><![CDATA[
\begin{code}
pCommand = pParallel

pParallel = do
  ps <- sepBy1 pSequence (pVarsym "<|>")
  return$ foldr1 (Z21.CommandLanguage.<|>) ps
\end{code}
]]></code>


Thus the sequential operator binds stronger than the parallel operator.

<code><![CDATA[
\begin{code}
pSequence = do
  ps <- sepBy1 pAtomicCommand (pVarsym "<+>")
  return$ foldr1 (Z21.CommandLanguage.<+>) ps
\end{code}
]]></code>

Atomic commands are the commands that were defined in the section before. These are basic commands for driving a train (<tt>fahre</tt>, <tt>halte</tt>), the 
command for switching a turn out (<tt>schalteWeiche</tt>), commands for waiting and synchronizing at events, the call of an macro definition and command in parantheses. 
<code><![CDATA[
\begin{code}
pAtomicCommand = 
      pHalte
  <|> pFahreLokSpeed 
  <|> pSchalteWeiche
  <|> ident "macheNichts" macheNichts
  <|> pWarte 
  <|> pWenn
  <|> (identifier >>= (return.Macro))
  <|> pParCommand
\end{code}
]]></code>

All these parsers can easily be written done by use of the do notation.

<code><![CDATA[
\begin{code}
pFahreLokSpeed = do
  f <- ident "fahre" fahre
  dir <- pDirection
  loco <- integerNumber
  speed <- integerNumber
  return$ f dir loco (fromIntegral speed)

pHalte = do
  ident "halte" ()
  loco <- integerNumber
  return$ halte loco

pWeichenstellung =
  (ident "geradeaus" True) 
     Text.Parsec.Prim.<|>  
  (ident "abzweigung" False) 
     Text.Parsec.Prim.<|>  
  pBool
  
pBool = (constructor "True" True) 
         Text.Parsec.Prim.<|>  
        (constructor "False" False) 

pSchalteWeiche = do
  ident "schalteWeiche" ()
  nr <- integerNumber
  abzweig <- pWeichenstellung
  return $ schalteWeiche (fromIntegral nr) abzweig

pDirection = pBackward <|>  pForward

pBackward = constructor "Backward" Backward
pForward = constructor "Forward" Forward

pWarte = do
  ident "warte" ()
  sekunden <- integerNumber
  cmd <- pAtomicCommand
  return$ warte sekunden cmd

pPair = do 
  special "("
  x <- integerNumber
  special ","
  y <- integerNumber
  special ")"
  return (x,y)

pWenn = do
  ident "wenn" ()
  special "["
  contacts <- sepBy1 pPair (special ",")
  special "]"
  cmd <- pAtomicCommand
  return $wenn contacts cmd 

pParCommand = do
  special "("
  com <- pCommand
  special ")"
  return com
\end{code}
]]></code>
</paragraph>
<paragraph>
<title>Executing Scripts</title>
This module has a main function. A script file can be submitted as command line argument. The file we be parsed and then be executed.


We define a simple function for logging and a keep alive thread.
<code><![CDATA[
\begin{code}
logger msg = print msg >> return False

keepAlive sendF = do
  Control.Monad.forever
    $oneShotTimer (sendF LAN_GET_SERIAL_NUMBER>>return())
    $sDelay 10
\end{code}
]]></code>

The main function processes the arguments and amounts in a call of the function <tt>moin</tt>. This way it is possible to statr the function <tt>moin</tt> in the interpreter <tt>ghci</tt><footnote>›Moin‹ in northern parts of Germany ist used for ›Hello‹. I use it as almost ›main‹</footnote>.

<code><![CDATA[
\begin{code}
main = do 
  args <- getArgs
  if (length args < 1)
   then (putStrLn "usage: runZ21 skriptfile [host port]") else do
     
  let (host:portA:_) =
        if length args < 3
        then [_DEFAULT_CLIENT,show _DEFAULT_PORT]
        else (tail args)

  let skriptFile = head args
  moin skriptFile host portA
\end{code}
]]></code>  

In order to run a command, we need to install the network connection, create a global state and the event listener queue.

<code><![CDATA[
\begin{code}
moin skriptFile host portA = do  
  -- network stuff
  let port = fromInteger (read portA)
  sock <- socket AF_INET Datagram defaultProtocol
  bindAddr <- inet_addr "0.0.0.0"
  hostAddr <- inet_addr host

  bindSocket sock (SockAddrInet port bindAddr)  
  let addr = (SockAddrInet port hostAddr)  
  let sendF = sendMsg sock addr
     
  -- synchronized state variable
  state <- newMVar newState
 
  eventListener <- newEventListener
  addListener eventListener logger
  eventThread <- startEventListener eventListener sock
\end{code}
]]></code>  

A state variable is used to signify if execution of the command has finished. This is used to wait for the end of the execution. Afterwards we need to close the socket and end the program.

<code><![CDATA[
\begin{code}
  done <- newEmptyMVar
\end{code}
]]></code>  

We will parse the script. Then add the setting of the variable <tt>done</tt> at the end of the script. Then hte execution is started.

<code><![CDATA[
\begin{code}
  sendF LAN_SET_BROADCASTFLAGS
          {general=True,rbus=True,systemState=True}
  file <- readFile  skriptFile
  let presult = parse pSkript  skriptFile (lexAll skriptFile file) 
  either
    (print)
    (\x->(print x >>
      (run eventListener state sendF (fst x)
       ( snd x
        <+> IOCommand(sClose sock >> putMVar done () )))))
    presult
\end{code}
]]></code>  

We wait for the variable <tt>done</tt> to be set.

<code><![CDATA[
\begin{code}
  takeMVar done -- blocks till MVar is full
  print "All done"
\end{code}
]]></code>
</paragraph>
<paragraph>
<title>Example Script</title>

Here follows the script which petforms the same procedure as seen in a previous section.

<code lang="z21" class="crossing"><![CDATA[schalteWeg_1_4 =  schalteWeiche 2 abzweigung
  <+> schalteWeiche 4 geradeaus
  <+> schalteWeiche 1 abzweigung

schalteWeg_3_2 = schalteWeiche 3 abzweigung
  <+> schalteWeiche 4 abzweigung
  <+> schalteWeiche 2 geradeaus

schalteWeg_1_2_3_4 = schalteWeiche 1 geradeaus
  <+> schalteWeiche 2 geradeaus
  <+> schalteWeiche 3 geradeaus
  <+> schalteWeiche 4 geradeaus

      schalteWeg_1_4
  <+> warte 5 (fahre Backward 6 5)
  <+> wenn [(1,4)] (halte 6)
  <+> schalteWeg_3_2
  <+> warte 5 (fahre Backward 7 5)
  <+> wenn [(1,2)] (halte 7)
  <+> schalteWeg_1_2_3_4
  <+> warte 5
   (     fahre Forward 6 5 <+> wenn [(1,3)] (halte 6)
    <|>  fahre Forward 7 5 <+> wenn [(1,1)] (halte 7)
    <|>  wenn  [(1,3),(1,1)]
      (   schalteWeg_1_4
      <+> warte 5 (fahre Backward 7 5)
      <+> wenn [(1,4)] (halte 7)
      <+> schalteWeg_3_2
      <+> warte 5 (fahre Backward 6 5)
      <+> wenn [(1,2)] (halte 6)
      <+> schalteWeg_1_2_3_4
      <+> warte 5
        (     fahre Forward 6 5 <+> wenn [(1,1)] (halte 6)
          <|> fahre Forward 7 5 <+> wenn [(1,3)] (halte 7)
          <|> wenn [(1,3),(1,1)] (warte 5 macheNichts)
        )
      )
   )
]]></code>
</paragraph>
</paragraph>
</paragraph>
</module>

