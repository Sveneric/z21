<?xml version="1.0" encoding="UTF-8"?>
<module>
<paragraph>
<title>Main GUI Client</title>
Time to start everything in an main function. This is our main GUI entry point.


<haskell><![CDATA[
\begin{code}
module Main where
import Z21.Protocoll hiding(light,direction, speed)
import Z21.State
import Z21.Gui
import Z21.MessageEventListener
import Z21.Commuting
import Z21.Constants

--gui library
import Graphics.UI.Gtk

-- Network library
import Network.Socket hiding ( recv, recvFrom)

--concurrency
import Control.Concurrent
import Control.Concurrent.Timer
import Control.Concurrent.Suspend.Lifted

--prog args and such
import System.Environment
\end{code}
]]></haskell>


Before we start the program we define a function that will send a message to the control every 10 seconds. This is needed by the control unit. It assures that our client is still alive.

<code><![CDATA[
\begin{code}
keepAlive sendF = do
  sendF  LAN_GET_SERIAL_NUMBER
  repeatedTimer (sendF LAN_GET_SERIAL_NUMBER >>return ())$sDelay 10
\end{code}
]]></code>

<haskell><![CDATA[
\begin{code}
main :: IO ()
main = withSocketsDo $ do
  -- process arguments
  args <- getArgs
  let (host:portA:_) =
        if length args < 2
        then [_DEFAULT_CLIENT,show _DEFAULT_PORT]
        else args

  -- network stuff
  let port = fromInteger (read portA)
  sock <- socket AF_INET Datagram defaultProtocol
  let bindAddr = tupleToHostAddress (0,0,0,0)-- inet_addr "0.0.0.0"
  let hostAddr = tupleToHostAddress (192,168,178,111) --inet_addr host

  bind sock (SockAddrInet port bindAddr)  
  let addr = (SockAddrInet port hostAddr)  
  let sendF = sendMsg sock addr
      
  -- keep alive timer
  keepAliveThread <- keepAlive sendF

  -- synchronized state variable
  state <- newMVar newState

  -- make the gui
  initGUI

  gui <- newGuiControls

  eventListener <- newEventListener
  addListener eventListener (logging gui state)
  addListener eventListener $processMessage gui state sendF
  eventThread <- startEventListener eventListener sock
  
  commutingGui <- newCommutingGui
  mkLayoutCommutingGui commutingGui
  addCommutingGuiEvents commutingGui eventListener state sendF

  window2 <- windowNew
  set window2 [ containerBorderWidth := 10 ]
  set window2 [ containerChild := mainPanel commutingGui]

  window <- createGuiWindow gui state sendF

  window `onDestroy` do
    sendF  LOGOFF
    killThread eventThread
    stopTimer keepAliveThread
    close sock
    mainQuit

  --show the windows
  widgetShowAll window
  widgetShowAll window2

  --start the gui thread
  mainGUI
  close sock
\end{code}
]]></haskell>
</paragraph>
</module>
