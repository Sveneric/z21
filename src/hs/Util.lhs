<?xml version="1.0" encoding="UTF-8"?>
<module>

<paragraph>
<title>Utility Functions</title>
This module has been created to take all utility functions used within the project. Eventually just one such functions has been written. Most other functions were allready present in some standard library and needn't to be invented again.

<haskell><![CDATA[
\begin{code}
module Util where

splitNChunks n = takeWhile(not.null) . map(take n) . iterate(drop n)
\end{code}
]]></haskell>
</paragraph>
</module>
