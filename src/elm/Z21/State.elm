module Z21.State exposing(..)
import Tuple exposing (..)

type alias State =
    {currentLoco:Loco
    ,locos: List Loco
    ,hauptschalterOn:Bool
    ,turnouts: List (Int, Bool)
    ,log:String}

initState =
    {currentLoco=initLoco
    ,locos=[]
    ,hauptschalterOn=True
    ,log=""
    ,turnouts=[(1,False),(2,False),(3,False),(4,False),(5,False),(6,False)]}
    
type alias Loco = 
  { lid:Int
  , address:Int
  , steps:SpeedSteps
  , speed:Int
  , direction:Direction
  , light:Bool
  , functions:List Function--(Int, Bool)
  , name:String
  } 

type alias Function =
  { nr:Int
  , description:String
  , state:Bool
  }
         
    
initLoco =
  { lid=0
  , address=3
  , steps=S128
  , speed=0
  , direction=Forward
  , light=False
  , functions=
      List.map
        (\n->{nr=n,description="F "++String.fromInt n,state=False})
        (List.range 1 20)
  , name = ""
  } 
    
type Direction = Forward | Backward 

switchDirection x = case x of
  Forward -> Backward
  Backward -> Forward

isForward x  = case x of
  Forward -> True
  _ -> False

type LOC_MODE = DCC|MM  
isDCC x = case x of
  DCC -> True
  _ -> False
       
type FunctionSwitch = On|Off|Switch

switchFunction fnr loco =
   let
     fsw nr fs = case fs of
                    [] -> []
                    (x::xs) -> if first x == nr then (first x,not (second x))::xs else x::fsw nr xs
   in {loco|functions=fsw fnr loco.functions}

setFunction fnr v loco =
   let
     fsw nr fs = case fs of
                    [] -> []
                    (x::xs) -> if x.nr == nr then {x|state=v}::xs else x::fsw nr xs
   in {loco|functions=fsw fnr loco.functions}


isFunctionSet fnr loco  =
   let
     fsw nr fs = case fs of
                    [] -> False
                    (x::xs) -> if x.nr == nr then x.state  else fsw nr xs
   in fsw fnr loco.functions
       
type SpeedSteps = S14|S28|S128
    
