port module Main exposing(..)

import Z21.State exposing (..)

import Html exposing (..)
import Html.Attributes  exposing (..)
import Html.Events exposing (onClick,onInput, onCheck, on,onFocus)

import Browser
import Platform.Sub
import Http exposing (get,expectString,post,stringPart,multipartBody)

import Xml.Decode exposing (..)
import XmlParser  exposing (parse,format,Node(..),Attribute)


port sendMessage : String -> Cmd msg
port messageReceiver : (String -> msg) -> Sub msg

type Messages =
   Nop
  |Hauptschalter
  |GetTurnoutInfos
  |Received  (Result Http.Error String)
  |ChangeAddress  String
  |UpdateSpeed String
  |SetDirection Direction
  |Licht
  |FunctionToggle Int
  |ToggleTurnout Int
  |WebSocketReceived String
  |AnlageReceived  (Result Http.Error String)
  | SelectTrain Int

main : Program () State Messages
main = let cs = [] -- beispielkarten
  in Browser.application
      { init = \_ url navKey ->
            ( initState
            , Http.get { url ="/layouts/fenster.xml", expect = Http.expectString AnlageReceived}
            )
      , update = \msg m ->nachrichtenVerarbeitung msg m
      , view = anblick
      , subscriptions = \_ -> subscription
      , onUrlRequest = \_ -> Nop
      , onUrlChange = \_ -> Nop}

getTurnoutInfos = Cmd.batch (List.map (\n-> Http.get { url ="/message/LAN_X_GET_TURNOUT_INFO "++String.fromInt n, expect = Http.expectString Received})[1,2,3,4,5,6])
      
nachrichtenVerarbeitung msg m = case msg of
  Nop ->(m,Cmd.none)
  GetTurnoutInfos -> (m,getTurnoutInfos)
  Hauptschalter ->
    if m.hauptschalterOn
    then ({m|hauptschalterOn=False}
       ,Http.get { url ="/message/LAN_X_SET_TRACK_POWER_OFF", expect = Http.expectString Received}) 
    else ({m|hauptschalterOn=True}
       ,Http.get { url ="/message/LAN_X_SET_TRACK_POWER_ON", expect = Http.expectString Received})
  ToggleTurnout nr  ->
      let ts = m.turnouts
          io = isOn nr ts
      in case io of
             Nothing -> (m,Cmd.none)
             Just b -> ({m|turnouts=List.map (\(n,x)->if n==nr then (n,not b) else (n,x)) ts}
                       ,Http.get { url ="/message/LAN_X_SET_TURNOUT "++(String.fromInt nr)++" True "++ (Debug.toString b), expect = Http.expectString Received}) 
                        
  ChangeAddress n ->
    let a = Maybe.withDefault m.currentLoco.address (String.toInt n)
        cl = m.currentLoco
        loco ={cl|address=a}
    in ({m|currentLoco=loco},Http.get { url ="/message/LAN_X_GET_LOCO_INFO "++String.fromInt a, expect = Http.expectString Received}) 
  UpdateSpeed n ->
    let speed = String.toInt n |> Maybe.withDefault m.currentLoco.speed
        cl = m.currentLoco
        loco ={cl|speed=speed}
    in ({m|currentLoco=loco}
       ,Http.get { url ="/message/LAN_X_SET_LOCO_DRIVE{locID="++String.fromInt (loco.address)++",steps=2,speed="++String.fromInt speed++",direction="++(Debug.toString loco.direction)++"}"
                 , expect = Http.expectString Received})

  (Received (Ok s)) -> (m,Cmd.none)
  (Received (Err s)) -> ({m|log=Debug.toString s},Cmd.none)
                        
  (AnlageReceived (Ok s)) -> case anlageFromXML s of
                                 (Ok tr) -> ({m|currentLoco=Maybe.withDefault initLoco (List.head tr)
                                             ,locos=tr
                                             ,log="Anlage geparst mit "++Debug.toString (List.length tr)++" Zügen"},Cmd.none)
                                 (Err e) -> ({m|log=Debug.toString e},Cmd.none)
  (AnlageReceived (Err s)) -> ({m|log=Debug.toString s},Cmd.none)

  SetDirection d ->
    let cl = m.currentLoco
        loco ={cl|direction=d}
    in ({m|currentLoco=loco}
       ,Http.get { url ="/message/LAN_X_SET_LOCO_DRIVE{locID="++String.fromInt (loco.address)++",steps=2,speed="++String.fromInt loco.speed++",direction="++(Debug.toString loco.direction)++"}"
                 , expect = Http.expectString Received})
  Licht ->
    let
      x = not m.currentLoco.light  
      cl = m.currentLoco
      loco ={cl|light=x}
    in({m|currentLoco=loco}
       ,Http.get { url ="/message/LAN_X_SET_LOCO_FUNCTION{locID="++String.fromInt loco.address++",switch="++(if x then "On" else "Off")++",index=0}"
                 , expect = Http.expectString Received})
  FunctionToggle n ->
    let
      x = not (isFunctionSet n m.currentLoco)
      cl = m.currentLoco
    in({m|currentLoco=setFunction n x cl}
       ,Http.get { url ="/message/LAN_X_SET_LOCO_FUNCTION{locID="++String.fromInt cl.address++",switch="++(if x then "On" else "Off")++",index="++String.fromInt n++"}"
                 , expect = Http.expectString Received})

  WebSocketReceived z21msg  -> prcocessZ21Msg m (String.words z21msg)

  SelectTrain ad ->
      let get xs = case xs of
                       [] -> Nothing
                       (l::ls) -> if l.address==ad then Just l else get ls
      in case get m.locos of
             Nothing -> ({m|log="could not select: "++String.fromInt ad},Cmd.none)
             Just l ->({m|currentLoco=l,locos = List.map (\x->if x.address==currentLoco.address then l else x) m.locos}
                      ,Http.get { url ="/message/LAN_X_GET_LOCO_INFO "++String.fromInt l.address, expect = Http.expectString Received}) 
                               
prcocessZ21Msg m z21msg = case z21msg of
  ["LAN_X_TURNOUT_INFO",nrs, vs] ->
      let
          nr = Maybe.withDefault -1 (String.toInt nrs)
          v = Maybe.withDefault -1 (String.toInt vs)
      in
          ({m|log=Debug.toString z21msg,turnouts=List.map (\(n,x)->if n==nr then (n,v==1) else (n,x)) m.turnouts},Cmd.none)
  ["WEBSOCKET"] -> (m,getTurnoutInfos)
  ("LAN_X_LOCO_INFO"::tails) ->
      let
          rest = String.concat tails
          str1 = String.replace "{" "" rest
          str2 = String.replace "}" "" str1
          vs = List.map (\x->String.split "=" x) <| String.split "," str2
          getAdd ys = case ys of
            [] -> Nothing
            (["locID",ad]::_) -> String.toInt ad
            (_::rs) -> getAdd rs
          isCurrent = Maybe.withDefault False <|Maybe.map (\a -> a==m.currentLoco.address )(getAdd vs)
      in
        if isCurrent then ({m|log=Debug.toString z21msg,currentLoco=updateCurrent vs m.currentLoco},Cmd.none)   else ({m|log=Debug.toString z21msg},Cmd.none)
  _ -> (m,Cmd.none)


toBool xs = case xs of
                "True" -> True
                _ -> False
       
updateCurrent vs loco = case vs of
  [] -> loco
  (["light",b]::rs) -> updateCurrent rs {loco|light=toBool b}
  (["f1",b]::rs) -> updateCurrent rs (setFunction 1 (toBool b) loco)
  (["f2",b]::rs) -> updateCurrent rs (setFunction 2 (toBool b) loco)
  (["f3",b]::rs) -> updateCurrent rs (setFunction 3 (toBool b) loco)
  (["f4",b]::rs) -> updateCurrent rs (setFunction 4 (toBool b) loco)
  (["f5",b]::rs) -> updateCurrent rs (setFunction 5 (toBool b) loco)
  (["f6",b]::rs) -> updateCurrent rs (setFunction 6 (toBool b) loco)
  (["f7",b]::rs) -> updateCurrent rs (setFunction 7 (toBool b) loco)
  (["f8",b]::rs) -> updateCurrent rs (setFunction 8 (toBool b) loco)
  (["f9",b]::rs) -> updateCurrent rs (setFunction 9 (toBool b) loco)
  (["f10",b]::rs) -> updateCurrent rs (setFunction 10 (toBool b) loco)
  (["f11",b]::rs) -> updateCurrent rs (setFunction 11 (toBool b) loco)
  (["f12",b]::rs) -> updateCurrent rs (setFunction 12 (toBool b) loco)
  (["f13",b]::rs) -> updateCurrent rs (setFunction 13 (toBool b) loco)
  (["f14",b]::rs) -> updateCurrent rs (setFunction 14 (toBool b) loco)
  (["f15",b]::rs) -> updateCurrent rs (setFunction 15 (toBool b) loco)
  (["f16",b]::rs) -> updateCurrent rs (setFunction 16 (toBool b) loco)
  (["f17",b]::rs) -> updateCurrent rs (setFunction 17 (toBool b) loco)
  (["f18",b]::rs) -> updateCurrent rs (setFunction 18 (toBool b) loco)
  (["f19",b]::rs) -> updateCurrent rs (setFunction 19 (toBool b) loco)
  (["f20",b]::rs) -> updateCurrent rs (setFunction 20 (toBool b) loco)
  (["direction","Forward"]::rs) -> updateCurrent rs {loco|direction=Forward}
  (["direction","Backward"]::rs) -> updateCurrent rs {loco|direction=Backward}
--  (["speed",n]::rs) -> updateCurrent rs {loco|speed=Maybe.withDefault 0<|String.toInt n}
  (_::rs) -> updateCurrent rs loco
                               
radiobutton n value msg sel =
    label []
        [ input
            [ type_ "radio"
            , name n
            , onClick msg
            , checked sel
            ] []
        , text value
        ]

viewOption ad loco =
  option
    [ selected  (Maybe.withDefault ""(List.head (String.words ad)) == String.fromInt loco.address), value <| String.fromInt loco.address
    ]
    [ text <|  String.fromInt loco.address++" "++loco.name]
   


        
anblick : State -> {body:List (Html Messages),title:String}
anblick m =
  {title="Modellbahnsteuerung"
  ,body=
    [div
     [style "background-image" ("url('layouts/images/b"++String.fromInt m.currentLoco.address++".jpg')"), style "width" "100%", style "height" "100%",style"background-repeat" "no-repeat",style"background-attachment" "fixed", style "background-size" "cover"]
     [ div
       [style "background-color" "rgba(255, 255, 255, 0.7)",style "height" "100%"]    
       ( [ h1[][text "Die Internet Modellbahn"]
         , button [onClick Hauptschalter][text "Hauptschalter"]
         , button [onClick GetTurnoutInfos][text "Weichenstatus Abfragen"]
         , p[][]
         , select
              [ onInput (\x -> SelectTrain (Maybe.withDefault (-1) (String.toInt x)))]
              (List.map (viewOption (String.fromInt m.currentLoco.address))  m.locos)
         , span [][text "Adresse: "]
         , input
           [ style "width" "50px"
           , style "height" "20pt"
           , onInput  (\n -> (ChangeAddress n))
           , value (String.fromInt m.currentLoco.address)
           , placeholder "Adresse" ]
           []
         ,p[][]
         ,img[src ("layouts/images/middle-b"++String.fromInt m.currentLoco.address++".jpg")][]
     
         , fieldset [style "width" "280px" ]
            [ text "Richtung: "
            , radiobutton "richtung" "Backward" (SetDirection Backward) (m.currentLoco.direction==Backward)
            , radiobutton "richtung" "Forward" (SetDirection Forward) (m.currentLoco.direction==Forward)
            ]
         , input
            [ type_ "range"
            , Html.Attributes.min "0"
            , Html.Attributes.max "27"
            , value <| String.fromInt (m.currentLoco.speed)
            , onInput UpdateSpeed
            , style "width" "280px"
            ]
               []
         , span [][text (String.fromInt m.currentLoco.speed)]
         , p[][]
         , button [onClick Licht,style "background-color" (if m.currentLoco.light then "#4CAF50" else "FFAAAA")][text "Licht"]
         , p[][]
         ]++List.map
             (\f->
                  button
                  [onClick (FunctionToggle f.nr),style "background-color" (if isFunctionSet f.nr m.currentLoco then "#4CAF50" else "FFAAAA")]
                  [text f.description])
             m.currentLoco.functions
             ++
             [p[][]
             , img [src "http://kueken.chickenkiller.com:8081/",alt "Livebild der Anlage"][]
             , p[][]
             , span [][text m.log]    
             ]
             ++
--             List.map turnout (List.range 1 6)
               [h3[][text "Stellwerk"]]
             ++weichenstrasse m

       )
     ]
    ]
    
  }


subscription = Platform.Sub.batch [messageReceiver WebSocketReceived]

weiche nr name {turnouts}
  = let io = Maybe.withDefault False (isOn nr turnouts)
    in td[style "padding" "0px"]
      [img [src ("images/"++name++(if io then "A.png" else "G.png")),onClick (ToggleTurnout nr)][]]
               
weichenstrasse m =
    [table
      [style "background-color" "#ffffff",style "border" "0px",style "margin" "0px", style "border-spacing" "0",style "border-collapse" "collapse"]
      [tr[style "padding" "0px"]
         [td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ,weiche 6 "linksUnten" m 
         ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ]
      ,tr[]
         [td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ,weiche 3  "linksUnten" m
         ,weiche 1  "rechtsUnten" m
         ,weiche 5  "linksOben" m
         ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ]
      ,tr[]
          [td[style "padding" "0px"][img [src "images/gerade.png"][]]
          ,weiche 4  "linksOben" m
          ,weiche 2  "rechtsOben" m
          ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
          ,weiche 7  "linksUnten" m
          ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
          ]
      ,tr[style "padding" "0px"]
         [td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ,td[style "padding" "0px"][img [src "images/gerade.png"][]]
         ,td[style "padding" "0px"][img [src "images/linksO.png"][]]
         ,td[][text ""]
         ]
       ]
    ]
{--
               
turnout nr =
  let name = "Weiche"++String.fromInt nr
  in                      
    div
      []
      [ fieldset [style "width" "280px" ]
        [ text "Weiche "
        , text (String.fromInt nr)
        , text ":"
        , radiobutton name "Geradeaus" (Turnout nr False) False 
        , radiobutton name "Abzweigung" (Turnout nr True) True
        ]
     ]               
--}

isOn nr ts = case ts of
               [] -> Nothing
               ((n,x)::xs) -> if n==nr then Just x else isOn nr xs

        


                        
anlageFromXML xml = run anlageDecoder xml
anlageDecoder = (path [ "trains"] (Xml.Decode.single trainListDecoder))               
                    
trainListDecoder = (path [ "train"] (Xml.Decode.list trainDecoder))               
trainDecoder
 =  Xml.Decode.map3
      (\a n fs-> {initLoco
                 |address=a
                 ,name=n
                 ,functions=fs})
      (Xml.Decode.intAttr "address")
      (path [ "name"] (Xml.Decode.single Xml.Decode.string))
      (path ["functions","function"](Xml.Decode.list functionDecoder))

stringAttrDecode s =
  Xml.Decode.withDefault "" (Xml.Decode.map (\x->x) (stringAttr s)) 

      
functionDecoder
  = Xml.Decode.map2
     (\nr des -> {nr=nr,description=des,state=False})
     (Xml.Decode.intAttr  "nr")
     (path [ "description"] (Xml.Decode.single Xml.Decode.string))
    
                   

                              
