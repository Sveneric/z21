# z21
An experimenmtal model railway control software for Roco/Fleischmann Z21 control units.

This project started as a little exercise in Haskell. The goal is to control a digital model railway. As server a z21 control unit is used. Z21 and z21 are products of Modelleisenbahn GmbH. They can be pruchased by Roco and Fleischmann model railways.

The z21 control unit communicates with clients by means of UDP. 
The here presented program is able to control locomotives. Furthermore it is possible to  program automatic sequences as trains commuting between two points.  

For the GUI parts the gtk2hs Haskell port of gtk is used. 


The Makefile of this project is only used to build the documentation from the literate Haskell sources.

As build tool cabal is used.
