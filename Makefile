#this is not the Makefile for compilation and packaging, but solely for the latex documentation of the code

SRC_FOLDER=src/hs/
DOC_FOLDER=doc

FILES = Z21/Constants Z21/Gui Z21/Protocoll Z21/State Z21/MessageEventListener Z21/Commuting Z21/CommandLanguage Client Util Server CommandParser
SOURCES = $(FILES:%=$(SRC_FOLDER)/%.lhs)
TEX_FILES = $(FILES:%=$(SRC_FOLDER)/%.tex)
LATEX=xelatex


%.tex: %.lhs xsl/sectionToLatex.xsl xsl/commonPrintTagsForLatex.xsl
	xsltproc  -output $@  xsl/sectionToLatex.xsl $<


doc/main.pdf: $(TEX_FILES) doc/main.xml doc/main.tex doc/bib.bib
	cp $(SRC_FOLDER)/*.tex $(SRC_FOLDER)/Z21/*.tex $(DOC_FOLDER)
	$(LATEX)  -output-directory=doc -input-directory=doc main.tex
	cd doc;bibtex main;cd ..
	$(LATEX)  -output-directory=doc -input-directory=doc main.tex


doc/main.tex: doc/main.xml
	xsltproc  -output doc/main.tex  xsl/sectionToLatex.xsl doc/main.xml
